# README #

This repo is for gathering all my work as a DevOp working with Ansible and ServerSpec for automation and TDD/BDD for infra development.

I arrange my files per intention and are self-explanatory (i hope):

* playbooks/
    - ad-hoc/
    - deploying/
    - facts/
    - inventory/
    - orchestration/
    - provisioning/
    - roles/
    - wip/
* spec/

# Targets #

My regular day to day targets are:

* GNU/Linux systems.
    - Fedora/Centos/Red Hat Enterprise Linux.
    - Debian/Ubuntu.
* Web servers.
* Web/cache proxy servers.
* HA and load balancing.
* Mail and collaboration servers.
* ERP and CRM systems.
* LDAP/IPA systems.
* Firewalling.
* Project Management systems.
* Source Code Management/Version Control.
* Instant Messaging servers.
* Rails Apps deploying.
