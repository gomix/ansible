require 'spec_helper'

describe command('ls /tmp') do
  it { should return_stdout 'foo' }
  it { should return_stderr 'bar' }
  it { should return_exit_status 0 }
end
