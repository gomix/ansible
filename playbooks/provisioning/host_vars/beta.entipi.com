---
fqdn: alpha.entipi.com
ipv4_0: 10.0.0.3
vms:
  dhcp-2.entipi.com:
    vm_name: dhcp-2.entipi.com
    fqdn: cockpit-1.entipi.com
    ipv4_0: 10.0.2.20
    mac_0: 52:54:00:c6:01:00
    ks_url: http://swrepo_1.entipi.com/ks/dhcp-2.entipi.com.ks
    ram: 256
    hdd0: 10
  test-2.entipi.com:
    vm_name: test-2.entipi.com
    fqdn: test-2.entipi.com
    ipv4_0: 10.0.2.3
    mac_0: 52:54:00:c6:01:01
    ks_url: http://swrepo_1.entipi.com/ks/test-2.entipi.com.ks
    ram: 512
    hdd0: 10

