---
ansible_ssh_host: ds_2.entipi.com
fqdn: ds_2.entipi.com
ipv4_0: 10.0.1.39
mac_0: 52:54:00:c6:72:92
ks_url: 'http://swrepo_1.entipi.com/ks/ds_2.ks'
