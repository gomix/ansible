---
ansible_ssh_host: ds_1.entipi.com
fqdn: ds_1.entipi.com
ipv4_0: 10.0.1.6
mac_0: 52:54:00:3e:19:82
ks_url: http://swrepo_1.entipi.com/ks/ds-1.entipi.com.ks
