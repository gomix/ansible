#version=RHEL7
# System authorization information
auth --enableshadow --passalgo=sha512

# Use CDROM installation media
cdrom

# Use text mode install
text

firstboot --disable
eula --agreed
ignoredisk --only-use=vda

# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'

# System language
lang es_VE.UTF-8

# Network information
network  --bootproto=dhcp --device=eth0 --noipv6 
network  --hostname=cadcam_1.entipi.com

# Root password
# cadcam2016 
#[ansible@ansible_1 playbooks]$ python -c 'import crypt; print crypt.crypt("cadcam2016", "$1$SomeSalt$")'
rootpw --iscrypted $1$SomeSalt$5zM9P9pl02RRGF.tgxxYL/
# ansible49516371
user --name=ansible --gecos=ansible_user --groups=wheel --password=$1$SomeSalt$qmQRE7z3l7oWsqoNwD2wv/ --iscrypted

# Do not configure the X Window System
skipx

# System timezone
timezone America/Caracas --isUtc

# System bootloader configuration
bootloader --append=" crashkernel=auto" --location=mbr --boot-drive=vda
autopart --type=lvm

# Partition clearing information
clearpart --all --initlabel --drives=vda

%packages
@base
@core
@gnome-desktop
@desktop-debugging
@dial-up
@directory-client
@fonts
@guest-agents
@guest-desktop-agents
@input-methods
@internet-browser
@java-platform
@multimedia
@network-file-system-client
@networkmanager-submodules
@print-client
@x11
firefox
kexec-tools

%end

%addon com_redhat_kdump --enable --reserve-mb='auto'

%end

reboot
