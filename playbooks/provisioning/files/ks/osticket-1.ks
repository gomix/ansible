#version=DEVEL
# System authorization information
auth --enableshadow --passalgo=sha512
# Use CDROM installation media
cdrom
# Use text mode install
text
# Run the Setup Agent on first boot
firstboot --enable
ignoredisk --only-use=vda
# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'
# System language
lang en_US.UTF-8

# Network information
network  --bootproto=dhcp --device=eth0 --noipv6
network  --hostname=localhost.localdomain

# Root password
rootpw --iscrypted $6$wVOaOyvhgD2ZS5iW$jHgk6UWOJLB0dplwFU53YObmdnUjFC9R0H/LERPBCAP7.boYd.MdeMrVw0p2AQBQTt4U9Itr/6CSnbKttyyHK0
# Do not configure the X Window System
skipx
# System timezone
timezone America/Caracas --isUtc
user --groups=wheel --name=ansible --password=$6$Z.cWA0CNTvMWic7L$RI.c1VEvIjOSDLvDkYOtUCP4zlmG5vW5RxS2tb8uTd/ASJFViLudsIJtRpD1mS89iSRjzv0y3.zxV/IKl0Eef/ --iscrypted --gecos="Ansible"
# System bootloader configuration
bootloader --append=" crashkernel=auto" --location=mbr --boot-drive=vda
autopart --type=lvm
# Partition clearing information
clearpart --all --initlabel --drives=vda

%packages
@core
kexec-tools

%end

%addon com_redhat_kdump --enable --reserve-mb='auto'

%end

%post
#---- Install our SSH key ----
mkdir -m0700 /home/ansible/.ssh/

cat <<EOF >/home/ansible/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC3+SyLotIWeWQrBDbHXdw7bLvNhTiOK44TdlkzoRE6iGNO8RenyD3pzLYuHqSsLInOdqLNZgTdtfl4uWVnteTEo5fscNHYPJDYKGskAfbPOvUORsfVDBOsnl5mkBnAsqyjxFaqqUG2GSo2CnpeFYDH3KwyqitH+qc/BdqitGK4g2zaKnaA4Bb5n3zugC0rU/gfeuqdbBtPs4PEGPDxuxDKzbQFRNvp/Gx179h7KuchCkWSalTHAPEXYGis8S4sznWx3Vi/YJeuMNWXKTtlnQgKJiDgml56EdfNEl+8WO51fpzTRWNuMyEc6DNqgNwhs9+NkuWRZUxy4n9qqOnuGCm3 gomix@kvm-1.localdomain
EOF

### set permissions
chmod 0600 /home/ansible/.ssh/authorized_keys
chown -R ansible.ansible /home/ansible

### fix up selinux context
restorecon -R /home/ansible/.ssh/

%end
