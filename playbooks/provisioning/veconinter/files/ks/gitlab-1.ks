#version=DEVEL
# System authorization information
auth --enableshadow --passalgo=sha512
# Use CDROM installation media
cdrom
# Use text mode install
text
# Run the Setup Agent on first boot
firstboot --enable
ignoredisk --only-use=vda
# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'
# System language
lang en_US.UTF-8

# Network information
network  --bootproto=dhcp --device=eth0 --noipv6
network  --hostname=localhost.localdomain

# Root password
rootpw --iscrypted $6$wVOaOyvhgD2ZS5iW$jHgk6UWOJLB0dplwFU53YObmdnUjFC9R0H/LERPBCAP7.boYd.MdeMrVw0p2AQBQTt4U9Itr/6CSnbKttyyHK0
# Do not configure the X Window System
skipx
# System timezone
timezone America/Caracas --isUtc
user --groups=wheel --name=ansible --password=$6$Z.cWA0CNTvMWic7L$RI.c1VEvIjOSDLvDkYOtUCP4zlmG5vW5RxS2tb8uTd/ASJFViLudsIJtRpD1mS89iSRjzv0y3.zxV/IKl0Eef/ --iscrypted --gecos="Ansible"
# System bootloader configuration
bootloader --append=" crashkernel=auto" --location=mbr --boot-drive=vda
autopart --type=lvm
# Partition clearing information
clearpart --all --initlabel --drives=vda

%packages
@core
kexec-tools

%end

%addon com_redhat_kdump --enable --reserve-mb='auto'

%end
