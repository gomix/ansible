---
fqdn: beta.entipi.com
ipv4_0: 10.0.0.3
vms:
  dhcp-2.entipi.com:
    vm_name: dhcp-2.entipi.com
    fqdn: dhc-2.entipi.com
    ipv4_0: 10.0.2.6
    mac_0: 52:54:00:3e:19:gg
    ks_url: http://swrepo_1.entipi.com/ks/ds-2.entipi.com.ks
    ram: 1096
    hdd0: 10
  test-2.entipi.com:
    vm_name: test-2.entipi.com
    fqdn: test-2.entipi.com
    ipv4_0: 10.0.2.3
    mac_0: 52:54:00:c6:01:01
    ks_url: http://swrepo_1.entipi.com/ks/test-2.entipi.com.ks
    ram: 512
    hdd0: 10

