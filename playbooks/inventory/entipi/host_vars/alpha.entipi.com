---
fqdn: alpha.entipi.com
ipv4_0: 10.0.0.2
libvirt_vg_0: centos
vms:
  ntp-1.entipi.com:
    vm_name: ntp-1.entipi.com
    fqdn: ntp-1.entipi.com
    ipv4_0: 10.0.1.2
    mac_0: 52:54:00:f4:a0:fd
    ks_url: http://swrepo_1.entipi.com/ks/ntp-1.entipi.com.ks
    ram: 256
    hdd0: 10
  ntp-2.entipi.com:
    vm_name: ntp-2.entipi.com
    fqdn: ntp_2.entipi.com
    ipv4_0: 10.0.1.3
    mac_0: 52:54:00:88:18:51 
    ks_url: http://swrepo_1.entipi.com/ks/ntp-2.entipi.com.ks
    ram: 256
    hdd0: 10
  dhcp-1.entipi.com:
    vm_name: dhcp-1.entipi.com
    fqdn: dhcp_1.entipi.com
    ipv4_0: 10.0.1.4
    mac_0: 52:54:00:f3:3c:5d
    ks_url: http://swrepo_1.entipi.com/ks/dhcp-1.entipi.com.ks
    ram: 256
    hdd0: 10
  ds_1:
    vm_name: ds_1
    fqdn: ds_1.entipi.com
    ipv4_0: 10.0.1.6
    mac_0: 52:54:00:3e:19:82
    ks_url: http://swrepo_1.entipi.com/ks/ds_1.ks
    ram: 1096
    hdd0: 10
  ds_2:
    vm_name: ds_2
    fqdn: ds_2.entipi.com
    ipv4_0: 10.0.1.39
    mac_0: 52:54:00:c6:72:92
    ks_url: http://swrepo_1.entipi.com/ks/ds_2.ks
    ram: 1096
    hdd0: 10
  zimbra_2:
    vm_name: zimbra_2
    fqdn: zimbra_2.entipi.com
    ipv4_0: 10.0.1.34
    mac_0: 52:54:00:c6:72:90
    ks_url: http://swrepo_1.entipi.com/ks/zimbra_2.ks
    ram: 8192 
    hdd0: 10
  zimbra-1.entipi.com:
    vm_name: zimbra-1.entipi.com
    fqdn: zimbra-1.entipi.com
    ipv4_0: 10.0.1.33
    mac_0: 52:54:00:c6:72:8f
    ks_url: http://swrepo_1.entipi.com/ks/zimbra-1.entipi.com.ks
    ram: 8192 
    hdd0: 10
  cadcam_1:
    vm_name: cadcam_1
    fqdn: cadcam_1.entipi.com
    ipv4_0: 10.0.1.42
    mac_0: 52:54:00:c6:72:94
    ks_url: http://swrepo_1.entipi.com/ks/cadcam_1.ks
    ram: 8192 
    hdd0: 10
  cadcam_2:
    vm_name: cadcam_2
    fqdn: cadcam_2.entipi.com
    ipv4_0: 10.0.1.43
    mac_0: 52:54:00:c6:72:95
    ks_url: http://swrepo_1.entipi.com/ks/cadcam_2.ks
    ram: 8192 
  generic_1.entipi.com:
    vm_name: generic_1.entipi.com
    fqdn: generic_1.entipi.com
    ipv4_0: 10.0.1.47
    mac_0: 52:54:00:c6:72:96
    ks_url: http://swrepo_1.entipi.com/ks/generic_1.entipi.com.ks
    ram: 8192 
    hdd0: 10
  samba-ad-1.entipi.com:
    vm_name: samba-ad-1.entipi.com
    fqdn: samba-ad-1.entipi.com
    ipv4_0: 10.0.1.45
    mac_0: 52:54:00:c6:72:97
    ks_url: http://swrepo_1.entipi.com/ks/samba-ad-1.entipi.com.ks
    ram: 8192 
    hdd0: 20
  builder-1.entipi.com:
    vm_name: builder-1.entipi.com
    fqdn: builder-1.entipi.com
    ipv4_0: 10.0.1.49
    mac_0: 52:54:00:c6:72:99
    ks_url: http://swrepo_1.entipi.com/ks/builder-1.entipi.com.ks
    ram: 8192 
    hdd0: 20
  ns-1.entipi.com:
    vm_name: ns-1.entipi.com
    fqdn: ns-1.entipi.com
    ipv4_0: 10.0.1.48
    mac_0: 52:54:00:c6:72:98
    ks_url: http://swrepo_1.entipi.com/ks/ns-1.entipi.com.ks
    ram: 1096
    hdd0: 10
  ipa-1.entipi.com:
    vm_name: ipa-1.entipi.com
    fqdn: ipa-1.entipi.com
    ipv4_0: 10.0.1.50
    mac_0: 52:54:00:c6:72:9a
    ks_url: http://swrepo_1.entipi.com/ks/ipa-1.entipi.com.ks
    ram: 2192
    hdd0: 20
  samba-sfs-1.entipi.com:
    vm_name: samba-sfs-1.entipi.com
    fqdn: samba-sfs-1.entipi.com
    ipv4_0: 10.0.1.51
    mac_0: 52:54:00:c6:72:9b
    ks_url: http://swrepo_1.entipi.com/ks/samba-sfs-1.entipi.com.ks
    ram: 8192
    hdd0: 10
  gomix-redmine.entipi.com:
    vm_name: gomix-redmine.entipi.com
    fqdn: gomix-redmine.entipi.com
    ipv4_0: 10.0.1.52
    mac_0: 52:54:00:c6:72:9c
    ks_url: http://swrepo_1.entipi.com/ks/gomix-redmine.entipi.com.ks
    ram: 1096
    hdd0: 10
  samba-sfs-2.entipi.com:
    vm_name: samba-sfs-2.entipi.com
    fqdn: samba-sfs-2.entipi.com
    ipv4_0: 10.0.1.53
    mac_0: 52:54:00:c6:72:9d
    ks_url: http://swrepo_1.entipi.com/ks/samba-sfs-2.entipi.com.ks
    ram: 1096
    hdd0: 10
  im-1.entipi.com:
    vm_name: im-1.entipi.com
    fqdn: im-1.entipi.com
    ipv4_0: 10.0.1.54
    mac_0: 52:54:00:c6:72:9e
    ks_url: http://swrepo_1.entipi.com/ks/im-1.entipi.com.ks
    ram: 4096
    hdd0: 20
  cockpit-1.entipi.com:
    vm_name: cockpit-1.entipi.com
    fqdn: cockpit-1.entipi.com
    ipv4_0: 10.0.1.55
    mac_0: 52:54:00:f4:a0:9f
    ks_url: http://swrepo_1.entipi.com/ks/cockpit-1.entipi.com.ks
    ram: 256
    hdd0: 10
  gt-1.entipi.com:
    vm_name: gt-1.entipi.com
    fqdn: gt-1.entipi.com
    ipv4_0: 10.0.1.56
    mac_0: 52:54:00:c6:72:9f
    ks_url: http://swrepo_1.entipi.com/ks/gt-1.entipi.com.ks
    ram: 2096
    hdd0: 20
  mail-2.entipi.com:
    vm_name: mail-2.entipi.com
    fqdn: mail-2.entipi.com
    ipv4_0: 10.0.1.16
    mac_0: 52:54:00:f4:a0:a1
    ks_url: http://swrepo_1.entipi.com/ks/mail-2.entipi.com.ks
    ram: 256
    hdd0: 20
  cnlt-1:
    vm_name: cnlt-1
    fqdn: cnlt-1.entipi.com
    ipv4_0: 10.0.1.58
    mac_0: 52:54:00:c6:72:a0
    ks_url: http://swrepo_1.entipi.com/ks/cnlt-1.ks
    vcpus: 2
    ram: 4096
    hdd0: 20
    os-variant: centos7.0
    os-type: linux
  otrs-1.entipi.com:
    vm_name: otrs-1.entipi.com
    fqdn: otrs-1.entipi.com
    ipv4_0: 10.0.1.56
    mac_0: 52:54:00:f4:a0:9g
    ks_url: http://swrepo_1.entipi.com/ks/otrs-1.entipi.com.ks
    ram: 2096
    hdd0: 20
  proxyr-2.entipi.com:
    vm_name: proxyr-2.entipi.com
    fqdn: proxyr-2.entipi.com
    ipv4_0: 10.0.1.35
    mac_0: 52:54:00:c6:72:91
    ks_url: http://swrepo_1.entipi.com/ks/proxyr-2.entipi.com.ks
    ram: 4096
    hdd0: 20

