---
ansible_ssh_host: 10.0.0.4
fqdn: gamma.entipi.com
ipv4_0: 10.0.0.4
vms:
  dhcp-3:
    vm_name: dhcp-3.entipi.com
    fqdn: dhc-3.entipi.com
    ipv4_0: 10.0.3.6
    mac_0: 52:54:00:3e:19:gg
    ks_url: http://swrepo_1.entipi.com/ks/ds-3.entipi.com.ks
    ram: 1096
    hdd0: 10
