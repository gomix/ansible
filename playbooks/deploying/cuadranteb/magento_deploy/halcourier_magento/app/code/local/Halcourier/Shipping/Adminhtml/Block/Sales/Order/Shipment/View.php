<?php
/**
 * ecommbits.com
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Halcourier
 * @package    Halcourier_Shipping
 * @version    Release: 1.0
 * @author     ecommbits.com (desarrollo@ecommbits.com)
 * @copyright  Copyright (c) 2013 ecommbits.com (http://www.ecommbits.com)
 */

class Halcourier_Shipping_Adminhtml_Block_Sales_Order_Shipment_View extends Mage_Adminhtml_Block_Sales_Order_Shipment_View
{
    public function __construct()
    {
        parent::__construct();
        $params = Mage::app()->getRequest()->getParams();
		$ship_id = $this->getShipment()->getId();
		$order_id = $this->getShipment()->getOrder()->getId();
		$halc = false;

        if(isset($params['halcourier_label']))
            $this->printLabel();

        if(isset($params['halcourier_delete_shipment']))
		{
			$url = $this->getUrl('*/sales_order/view/order_id/'.$order_id, array('store'=>$this->getRequest()->getParam('store', 0)));
			try {
				if($this->getShipment()->delete())
				{
					Mage::getSingleton('adminhtml/session')->addSuccess(
						Mage::helper('halcouriershipping')->__('El envío se ha eliminado correctamente.'));
				}
				else
				{
					Mage::getSingleton('adminhtml/session')->addError(
						Mage::helper('halcouriershipping')->__('El envío no se ha podido eliminar.'));
				}
				$this->_redirect($url);
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect($url);
                return;
            }
		}

        foreach ($this->getShipment()->getAllTracks() as $track)
            if ($track->getCarrierCode() == 'halcourier' || $track->getCarrierCode() == 'HALCOURIERShipping')
				$halc = true;

        if ($ship_id && $halc)
        {
			if($this->getShipment()->getShippingLabel())
        	{
	            $this->_addButton('halcourier_label', array(
	                'label'     => Mage::helper('halcouriershipping')->__('Etiqueta Halcourier'),
	                'class'     => 'go',
	                'onclick'   => 'window.open(\''.$this->getLabelHalcourierUrl().'\')'
	                ));
			}
        }

        $this->_addButton('halcourier_delete', array(
            'label'     => Mage::helper('halcouriershipping')->__('Eliminar Envío'),
            'class'     => 'delete',
            'onclick'   => 'if (confirm(\''.Mage::helper('halcouriershipping')->__('¿Confirma la eliminación del envío?').'\')) javascript:setLocation(\''.$this->getDeleteShipmentHalcourierUrl($order_id).'\'); return false;',
            ));
    }

    public function getLabelHalcourierUrl()
    {
        $url = '';
        foreach ($this->getShipment()->getAllTracks() as $track)
        {
            if ($track->getCarrierCode() == 'halcourier' || $track->getCarrierCode() == 'HALCOURIERShipping')
            {
                $url = Mage::helper('core/url')->getCurrentUrl();
                $url .= '?halcourier_label';
            }
        }
        return $url;
    }
	
    public function getDeleteShipmentHalcourierUrl($order_id)
    {
        $url = $this->getUrl('halcouriershipping/shipment/delete', array('shipment_id'  => $this->getShipment()->getId()));
		$url .= '?halcourier_delete_shipment='.$order_id;
        return $url;
    }

    public function updateBackButtonUrl($flag)
    {
        if ($flag) {
            if ($this->getShipment()->getBackUrl()) {
                return $this->_updateButton('back', 'onclick', 'setLocation(\'' . $this->getShipment()->getBackUrl() . '\')');
            }
            return $this->_updateButton('back', 'onclick', 'setLocation(\'' . $this->getUrl('*/sales_shipment/') . '\')');
        }
        return $this;
    }

    public function printLabel()
    {
		if($this->getShipment()->getShippingLabel())
        {
            $pdf = base64_decode($this->getShipment()->getShippingLabel());
            $shipment_id = $this->getShipment()->getIncrementId();
            header("Content-type: application/pdf");
            header('Content-Disposition: attachment; filename="'.Mage::helper('halcouriershipping')->__('etiqueta_').$shipment_id.'.pdf"');
            echo $pdf;
        }
    }
}
