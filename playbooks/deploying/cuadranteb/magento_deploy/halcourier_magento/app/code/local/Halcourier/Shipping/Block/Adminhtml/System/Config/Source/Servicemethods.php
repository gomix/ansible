<?php
/**
 * ecommbits.com
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Halcourier
 * @package    Halcourier_Shipping
 * @version    Release: 1.0
 * @author     ecommbits.com (desarrollo@ecommbits.com)
 * @copyright  Copyright (c) 2013 ecommbits.com (http://www.ecommbits.com)
 */

class Halcourier_Shipping_Block_Adminhtml_System_Config_Source_Servicemethods extends Mage_Adminhtml_Block_System_Config_Form_Field
{
	protected $_addRowButtonHtml = array();
	protected $_removeRowButtonHtml = array();

	protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
	{
		$this->setElement($element);
		$javaScript = "
            <script type=\"text/javascript\">
				function set(object) {
					var target = document.getElementById(object.id +'[' + object.value + ']');
					var container = document.getElementById('halcourier_tipos_servicio_container');
					var selects = container.getElementsByTagName('select');
					for (var i = 0; i < selects.length; i++) {
						selects[i].value = 0;
					}
					if(object.checked) {
				    	target.value = 1;
				    }
				    else {
						target.value = 0;
					}
				}
				function checkUnique() {
					var container = document.getElementById('halcourier_tipos_servicio_container');
					var selects = container.getElementsByTagName('select');
					var val = 0;
					var message = \"".$this->__('No es posible definir más de un tipo de servicio por defecto.')."\";
					for (var i = 0; i < selects.length; i++) {
						if(parseInt(selects[i].value))
							val += parseInt(selects[i].value);
					}
					if(val > 1) {
						alert(message);
						for (var i = 0; i < selects.length; i++) {
							if(parseInt(selects[i].value))
								selects[i].value = 0;
						}
				    }
				}
            </script>";
		$html = $javaScript;
 		$html .= '<div id="halcourier_tipos_servicio_template" style="display:none">';
		$html .= $this->_getRowTemplateHtml();
		$html .= '</div>';
 		$html .= '<ul id="halcourier_tipos_servicio_container">';
		if ($this->_getValue('tipos_servicio_codigo'))
		{
			foreach ($this->_getValue('tipos_servicio_codigo') as $i => $f)
			{
               if ($i)
                   $html .= $this->_getRowTemplateHtml($i);
			}
		}
		$html .= '</ul>';
		$html .= $this->_getAddRowButtonHtml('halcourier_tipos_servicio_container',
           'halcourier_tipos_servicio_template', $this->__('Añadir nuevo tipo de servicio'));
		
		$this->_getSelectMatrixShipmentMethods();
 
		return $html;
	}
 
	protected function _getRowTemplateHtml($rowIndex = 0)
	{
		$html = '<li>';
		$html .= '<div style="margin:5px 0 10px;">';
		$html .= '<input type="text" class="input-text" maxlength="4" style="width:32px;" name="'
			.$this->getElement()->getName().'[tipos_servicio_codigo][]" value="'
			.$this->_getValue('tipos_servicio_codigo/'.$rowIndex) . '" '.$this->_getDisabled().'/> ';
		$html .= '<input type="text" style="width:149px;" class="input-text" name="'
			.$this->getElement()->getName().'[tipos_servicio_descripcion][]" value="'
			.$this->_getValue('tipos_servicio_descripcion/'.$rowIndex).'" '.$this->_getDisabled().'/> ';
		$html .= '<select title="'.$this->__('Opción por defecto').'" class="select" onchange="checkUnique()" id="[tipos_servicio_defecto]['.$rowIndex.']" name="'.$this->getElement()->getName().'[tipos_servicio_defecto][]" '.$this->_getDisabled().' style="width:44px;height:21px;margin-right: 2px;">';
		$html .= '<option value="0" '.$this->_getSelected('tipos_servicio_defecto/' . $rowIndex, '0').'>'.$this->__('No').'</option>';
		$html .= '<option value="1" '.$this->_getSelected('tipos_servicio_defecto/' . $rowIndex, '1').'>'.$this->__('Sí').'</option>';
		$html .= '</select>';
		$html .= $this->_getRemoveRowButtonHtml('li', null);
		$html .= '<div style="height:2px;"></div>';
		$html .= $this->_getSelectShipmentMethodsHtml($rowIndex, $this->getElement()->getData('value/'.'metodo_envio/'.$rowIndex));
		$html .= '</div>';
		$html .= '</li>';

		return $html;
	}

	protected function _getDisabled()
	{
		return $this->getElement()->getDisabled() ? ' disabled' : '';
	}
 
	protected function _getValue($key)
	{
		return $this->getElement()->getData('value/' . $key);
	}
 
	protected function _getSelected($key, $value)
	{
		return $this->getElement()->getData('value/' . $key) == $value ? 'selected="selected"' : '';
	}
	
	protected function _getRadioSelected($key, $value)
	{
		return $this->getElement()->getData('value/' . $key) == $value ? 'checked="yes"' : '';
	}
 
	protected function _getAddRowButtonHtml($container, $template, $title='Add')
	{
		if (!isset($this->_addRowButtonHtml[$container]))
		{
			$this->_addRowButtonHtml[$container] = $this->getLayout()->createBlock('adminhtml/widget_button')
					->setType('button')
					->setClass('add ' . $this->_getDisabled())
					->setLabel($this->__($title))
					->setOnClick("Element.insert($('" . $container . "'), {bottom: $('" . $template . "').innerHTML})")
					->setDisabled($this->_getDisabled())
					->toHtml();
		}
		return $this->_addRowButtonHtml[$container];
	}
 
	protected function _getRemoveRowButtonHtml($selector = 'li', $title = 'Borrar')
	{
		if (!$this->_removeRowButtonHtml)
		{
			$this->_removeRowButtonHtml = $this->getLayout()->createBlock('adminhtml/widget_button')
				->setType('button')
				->setClass('delete v-middle ' . $this->_getDisabled())
				->setLabel($this->__($title))
				->setOnClick("Element.remove($(this).up('" . $selector . "'))")
				->setDisabled($this->_getDisabled())
				->toHtml();
		}
		return $this->_removeRowButtonHtml;
	}

	protected function _getSelectShipmentMethodsHtml($rowIndex, $default_value = '')
	{
		$shipping_methods = new Mage_Adminhtml_Model_System_Config_Source_Shipping_Allmethods();
		$shipping_methods = $shipping_methods->toOptionArray(true);

		if(Mage::getConfig()->getModuleConfig('Webshopapps_Matrixrate')->is('active', 'true'))
		{
			$shipping_methods = array_merge($shipping_methods, $this->_getSelectMatrixShipmentMethods());
		}

		$tmp = Mage::app()->getLayout()->createBlock('core/html_select')
		    ->setName($this->getElement()->getName().'[metodo_envio][]"'.$this->_getDisabled())
		    ->setId('[metodo_envio]['.$rowIndex.']')
		    ->setTitle($this->__('Métodos de envío'))
			->setValue($default_value)
		    ->setClass('select');
		$select = $tmp->setOptions($shipping_methods);

		return $select->getHtml();
	}
	
	protected function _getSelectMatrixShipmentMethods()
	{
		try
		{
			if(Mage::getConfig()->getModuleConfig('Webshopapps_Matrixrate')->is('active', 'true'))
			{
				$title = Mage::getStoreConfig('carriers/matrixrate/title', Mage::app()->getStore());
				$title_free_shipping = Mage::getStoreConfig('carriers/matrixrate/free_method_text', Mage::app()->getStore());
				$collection = Mage::getResourceModel('matrixrate_shipping/carrier_matrixrate_collection');
				$collection->getSelect()->group('delivery_type');
				$arr = array();
				$arr['matrixrate'] = array('label'=>$title);
				foreach ($collection->getData() as $row) {
				    $arr['matrixrate']['value'][] = array('value'=>'[matrixrate]'.$title.' - '.$row['delivery_type'], 'label'=>'[matrixrate] '.$row['delivery_type']);
				}
				$arr['matrixrate']['value'][] = array('value'=>'[matrixrate]matrixrate_matrixrate_free', 'label'=>'[matrixrate] '.$title_free_shipping);
				return $arr;
			}
			else
			{
				return false;
			}
		}
		catch(exception $e)
		{
			return false;
		}
	}
}