<?php
/**
 * ecommbits.com
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Halcourier
 * @package    Halcourier_Shipping
 * @version    Release: 1.0
 * @author     ecommbits.com (desarrollo@ecommbits.com)
 * @copyright  Copyright (c) 2013 ecommbits.com (http://www.ecommbits.com)
 */

class Halcourier_Shipping_Helper_Utils extends Mage_Core_Helper_Abstract
{
	public static function objectToArray($object)
	{
		if(is_array($object) || is_object($object)) {
			$array = array();
			foreach($object as $key => $value) {
				$array [$key]  = self::objectToArray($value);
			}
			return $array;
		}
		return $object;
	}
	
	public static function objectsToNusoap($auth, $serv, $typeIn)
	{
		$body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:mes="http://messagein.halsystem.sw" xmlns:com="http://complexType.halsystem.sw"><soapenv:Header/><soapenv:Body>';
		$body .= '<mes:' . $typeIn . '>';
		$body .= '<mes:autenticar>';
		foreach($auth as $key => $value) {
			$body .= '<com:' . $key . '>' . $value . '</com:' . $key . '>';
		}
		$body .= '</mes:autenticar>';
		foreach($serv as $key => $value) {
			if ($key != 'autenticar') {
				$body .= '<mes:' . $key . '>' . $value . '</mes:' . $key . '>';
			}
		}
		$body .= '</mes:' . $typeIn . '>';
		$body .= '</soapenv:Body></soapenv:Envelope>';
		return $body;
	}
	
    public static function objectsToNusoapBusquedasAuxiliares($auth, $serv, $typeIn) {
        $cuerpo = '';
        $cuerpo .= '<' . $typeIn . ' xmlns="http://tempuri.org/"><xbusquedaAuxiliares>';
        foreach($serv as $key => $value) {
            if ($key != 'Autenticacion') {
                $cuerpo .= '<' . $key . '>' . $value . '</' . $key . '>';
            }
        }
        $cuerpo .= '<Autenticacion>';
        foreach($auth as $key => $value) {
            $cuerpo .= '<' . $key . ' xmlns="http://complexType.halsystem.sw">' . $value . '</' . $key . '>';
        }
        $cuerpo .= '</Autenticacion>';
        $cuerpo .= '</xbusquedaAuxiliares></' . $typeIn . '>';
        return $cuerpo;
    }

	public static function objectsToNusoapComplex($auth, $serv, $typeIn)
	{
		$body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"'; 
		$body .= 'xmlns:mes="http://messagein.halsystem.sw" xmlns:com="http://complexType.halsystem.sw">';
		$body .= '<soapenv:Header/><soapenv:Body>';
		$body .= '<mes:' . $typeIn . '>';
		$body .= '<mes:autenticar>';
		foreach($auth as $key => $value) {
			$body .= '<com:' . $key . '>' . $value . '</com:' . $key . '>';
		}
		$body .= '</mes:autenticar>';
		foreach($serv as $key => $value) {
			if ($key != 'autenticar') {
				$body .= '<mes:' . $key . '>'; 
				if ($key != 'nomPlan') {
					foreach($value as $key2 => $value2) {
						$body .= '<com:' . $key2 . '>' . $value2 . '</com:' . $key2 . '>';
					}
				} else {
					$body .= $value;
				}
				$body .= '</mes:' . $key . '>';
			}
		}
		$body .= '</mes:' . $typeIn . '>';
		$body .= '</soapenv:Body></soapenv:Envelope>';
		return $body;
	}

	public static function objectsToNusoapImportacion($auth, $serv, $typeIn)
	{
		$body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:mes="http://messagein.halsystem.sw" xmlns:com="http://complexType.halsystem.sw"><soapenv:Header/><soapenv:Body>';
		$body .= '<mes:' . $typeIn . '>';
		$body .= '<mes:autenticar>';
		foreach($auth as $key => $value) {
			$body .= '<com:' . $key . '>' . $value . '</com:' . $key . '>';
		}
		$body .= '</mes:autenticar>';
		foreach($serv as $key => $value) {
			if ($key != 'autenticar') {
				if ($key != 'linea') {
					$body .= '<mes:' . $key . '>'; 
					foreach($value as $key2 => $value2) {
						$body .= '<com:' . $key2 . '>' . $value2 . '</com:' . $key2 . '>';
					}
					$body .= '</mes:' . $key . '>';
				} else {
					foreach($value as $key2 => $value2) {
						$body .= '<mes:' . $key . '>'; 
						$body .= $value2;
						$body .= '</mes:' . $key . '>';
					}
				}
			}
		}
		$body .= '</mes:' . $typeIn . '>';
		$body .= '</soapenv:Body></soapenv:Envelope>';
		return $body;
	}
    
	public static function cleanString($string)
	{
		$string = trim($string);
		$string = str_replace(
		    array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
		    array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
		    $string
		);
		
		$string = str_replace(
		    array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
		    array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
		    $string
		);
		
		$string = str_replace(
		    array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
		    array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
		    $string
		);
		
		$string = str_replace(
		    array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
		    array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
		    $string
		);
		
		$string = str_replace(
		    array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
		    array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
		    $string
		);
		
		$string = str_replace(
		    array('ñ', 'Ñ', 'ç', 'Ç'),
		    array('n', 'N', 'c', 'C'),
		    $string
		);
		
		$string = str_replace(
		    array('@', '#', '$', '€', '&'),
		    array('[a]', 'n', 'S', 'EUR', '&amp;'),
		    $string
		);
		
		$string = str_replace(
		    array("\\", "¨", "º", "~",
		         "|", "!", "\"", "·", "ª", "/",
		         "^", "`", "¨", "´", "¬", "%"),
		    ' ',
		    $string
		);

		return $string;
	}

	public static function ponerEspacios($valor, $long)
	{
		$blanco = " ";
		$valor = trim($valor);
		$longitud = strlen($valor);
		if ($longitud < $long) {
			for ($i=1; $i <= ($long - $longitud); $i++) {
				$valor .= $blanco;
			}
		}
		return $valor;
	}
	
	public static function ponerCeros($valor, $long)
	{
		$longitud = strlen($valor);
		if ($longitud < $long) {
			for ($i=1; $i <= ($long - $longitud); $i++) {
				$valor = '0' . $valor;
			}
		}
		return $valor;
	}
	
	public static function formatearFechaDeSw($fecha) 
	{
		if ($fecha.lenght>0) {
			$ano = $fecha[0].$fecha[1].$fecha[2].$fecha[3];
			$mes = $fecha[5].$fecha[6];
			$dia = $fecha[8].$fecha[9];
			return $dia.'/'.$mes.'/'.$ano;
		} else {
			return '';
		}
	}
	
	public static function formatearFecha($fecha)
	{
		if ($fecha.lenght>0) {
			$ano = $fecha[0].$fecha[1].$fecha[2].$fecha[3];
			$mes = $fecha[8].$fecha[9];
			$dia = $fecha[5].$fecha[6];
			return $dia.'/'.$mes.'/'.$ano;
		} else {
			return '';
		}
	}

	public static function formatearHora($hora)
	{
		if ($hora.lenght>0) {
			$hor = $hora[11].$hora[12];
			$min = $hora[14].$hora[15];
			return $hor.':'.$min;
		} else {
			return '';
		}
	}
	
	public static function formatearHoraDeSw($fecha)
	{
		if ($fecha.lenght>0) {
			$hora = $fecha[11].$fecha[12].$fecha[13].$fecha[14].$fecha[15];
			return $hora;
		} else {
			return '';
		}
	}
}

?>