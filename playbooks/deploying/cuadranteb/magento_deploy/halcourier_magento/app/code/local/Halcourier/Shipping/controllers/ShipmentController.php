<?php
/**
 * ecommbits.com
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Halcourier
 * @package    Halcourier_Shipping
 * @version    Release: 1.0
 * @author     ecommbits.com (desarrollo@ecommbits.com)
 * @copyright  Copyright (c) 2013 ecommbits.com (http://www.ecommbits.com)
 */
 
class Halcourier_Shipping_ShipmentController extends Mage_Adminhtml_Controller_Sales_Shipment
{
    public function deleteAction()
    {
		if ($shipment = $this->_initShipment())
		{
			$params = Mage::app()->getRequest()->getParams();
			if(isset($params['halcourier_delete_shipment']))
			{
				$url = $this->getUrl('adminhtml/sales_order/view', array('order_id'=>$params['halcourier_delete_shipment']));
				try {
					$items = $this->_getShipmentItems($shipment->getId());
					if($shipment->delete())
					{
						$this->_deleteShipmentItems($shipment, $items);
						Mage::getSingleton('adminhtml/session')->addSuccess(
							Mage::helper('halcouriershipping')->__('El envío se ha eliminado correctamente.'));
					}
					else
					{
						Mage::getSingleton('adminhtml/session')->addError(
							Mage::helper('halcouriershipping')->__('El envío no se ha podido eliminar.'));
					}
					Mage::app()->getResponse()->setRedirect($url);
					return;
				} catch (Exception $e) {
					Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
					Mage::app()->getResponse()->setRedirect($url);
					return;
				}
			}    		
    	}
    }

    protected function _initShipment()
    {
        $this->_title($this->__('Sales'))->_title($this->__('Shipments'));

        $shipment = false;
        $shipmentId = $this->getRequest()->getParam('shipment_id');
        $orderId = $this->getRequest()->getParam('order_id');
        if ($shipmentId) {
            $shipment = Mage::getModel('sales/order_shipment')->load($shipmentId);
        } elseif ($orderId) {
            $order      = Mage::getModel('sales/order')->load($orderId);

            /**
             * Check order existing
             */
            if (!$order->getId()) {
                $this->_getSession()->addError($this->__('The order no longer exists.'));
                return false;
            }
            /**
             * Check shipment is available to create separate from invoice
             */
            if ($order->getForcedDoShipmentWithInvoice()) {
                $this->_getSession()->addError($this->__('Cannot do shipment for the order separately from invoice.'));
                return false;
            }
            /**
             * Check shipment create availability
             */
            if (!$order->canShip()) {
                $this->_getSession()->addError($this->__('Cannot do shipment for the order.'));
                return false;
            }
            $savedQtys = $this->_getItemQtys();
            $shipment = Mage::getModel('sales/service_order', $order)->prepareShipment($savedQtys);

            $tracks = $this->getRequest()->getPost('tracking');
            if ($tracks) {
                foreach ($tracks as $data) {
                    if (empty($data['number'])) {
                        Mage::throwException($this->__('Tracking number cannot be empty.'));
                    }
                    $track = Mage::getModel('sales/order_shipment_track')
                        ->addData($data);
                    $shipment->addTrack($track);
                }
            }
        }

        return $shipment;
    }

    public function removeTrackAction()
    {
        $trackId    = $this->getRequest()->getParam('track_id');
        $shipmentId = $this->getRequest()->getParam('shipment_id');
        $track = Mage::getModel('sales/order_shipment_track')->load($trackId);
    	$log = Mage::helper('halcouriershipping')->getConfigData('log');
    	$deleteHalcourier = true;
        if ($track->getId()) {
            try {
                if ($this->_initShipment()) {
					if ($track->getCarrierCode() == 'halcourier')
                		$deleteHalcourier = $this->_deleteHalcourierShipment($track);
                	if(is_array($deleteHalcourier))
                		$response = $deleteHalcourier;
                	else
                	{
						$track->delete();
						$this->loadLayout();
						//$response = $this->getLayout()->getBlock('shipment_tracking')->toHtml();
						$response = $this->_toHtml();
                	}
                } else {
                    $response = array(
                        'error'     => true,
                        'message'   => $this->__('Cannot initialize shipment for delete tracking number.'),
                    );
                }
            } catch (Exception $e) {
                $response = array(
                    'error'     => true,
                    'message'   => $this->__('Cannot delete tracking number.').' '.$e->getMessage(),
                );
            }
        } else {
            $response = array(
                'error'     => true,
                'message'   => $this->__('Cannot load track with retrieving identifier.'),
            );
        }
        if (isset($response) && is_array($response)) {
            $response = Mage::helper('core')->jsonEncode($response);
        }
		//Mage::app()->getResponse()->setRedirect('adminhtml/sales_order_shipment/view', array('shipment_id' => $shipmentId));
        $this->getResponse()->setBody($response);
    }

	protected function _deleteHalcourierShipment($track)
	{
		$log = Mage::helper('halcouriershipping')->getConfigData('log');
		$shipment = $this->_initShipment();
		if ($track->getId())
		{
			if ($track->getCarrierCode() == 'halcourier')
			{
				$ordenId = $track->getOrderId();
				if(!Mage::helper('halcouriershipping')->loadClass('Soap/nusoap','require') ||
					!Mage::helper('halcouriershipping')->loadClass('Utils/Types') ||
					!Mage::helper('halcouriershipping')->loadClass('Utils'))
				{
					Mage::helper('halcouriershipping')->log('[BORRAR ENVIO][ERROR EN LA CARGA DE CLASES] PEDIDO #'.$ordenId,9);
				}
				$db = Mage::getSingleton('core/resource')->getConnection('core_write');
				$result = $db->query("select track_number from sales_flat_shipment_track where entity_id = '".$track->getId()."' and carrier_code = 'halcourier' limit 1");
				$row = $result->fetch();
				if(count($row) == 0)
					return true;
				$login = new LoginTypeIn();
				$login->codAgencia = Mage::helper('halcouriershipping')->getConfigData('halcourier_codigo_agencia', $shipment->getStoreId());
				$login->codCliente = Mage::helper('halcouriershipping')->getConfigData('halcourier_codigo_cliente', $shipment->getStoreId());
				$login->password = Mage::helper('core')->decrypt(Mage::helper('halcouriershipping')->getConfigData('halcourier_password_cliente', $shipment->getStoreId()));
				$login->tipoValidacion = 'Cliente';
				$client = new nusoap_client(Mage::getStoreConfig('carriers/halcourier_avanzadas/halcourier_url_envios', $shipment->getStoreId()),true);
				$envio = new BorrarEnvioTypeIn();
				$envio->autenticar = Halcourier_Shipping_Helper_Utils::objectToArray($login);
				$envio->albaran = $row['track_number'];
				$body = Halcourier_Shipping_Helper_Utils::objectsToNusoap($envio->autenticar,$envio,'BorrarEnvioTypeIn');
				$client->soap_defencoding = 'utf-8';
				$client->useHTTPPersistentConnection();
				$client->setUseCurl(true);
				$response = $client->send($body, 'BorrarEnvio');
				if($log)
				{
					Mage::helper('halcouriershipping')->log('[BORRAR ENVIO] '.print_r($envio,true),5);
					Mage::helper('halcouriershipping')->log('[RESPUESTA BORRAR ENVIO] '.print_r($response,true),5);
				}
				$errors = false;
				if(isset($response['return']) && is_array($response['return']))
				{
					if ($response['return']['codError'] != '0' && $response['return']['codError'] != '2')
					{
						$error_message = '['.$response['return']['codError'].'] '.$response['return']['descripcionError'];
						$errors = true;
					}
					else
					{
						Mage::getSingleton('adminhtml/session')->addSuccess(
							Mage::helper('halcouriershipping')->__('Halcourier: Albarán').' '.$row['track_number'].' '.Mage::helper('halcouriershipping')->__('eliminado correctamente.'));
					}
					if ($response['return']['codError'] == '2')
					{
						Mage::getSingleton('adminhtml/session')->getMessages(true);
						$errors = false;
						Mage::getSingleton('adminhtml/session')->addSuccess(
							Mage::helper('halcouriershipping')->__('Halcourier: El albarán').' '.$row['track_number'].' '.Mage::helper('halcouriershipping')->__('ya estaba eliminado en el sistema.'));
					}
				}
				else
				{
					if(isset($response['faultstring']))
						$error_message = '[99999] '.$response['faultstring'];
					else
						$error_message = '[99999] '.Mage::helper('halcouriershipping')->__('Error indeterminado. Compruebe la conexión con Halcourier.');
					$errors = true;
				}
				if ($errors == true)
				{
					$error_response = array(
						'error'     => true,
						'message'   => Mage::helper('halcouriershipping')->__('Halcourier error:').' '.$error_message,
					);
					return $error_response;
				}
				else
				{
					$result = $db->query("update sales_flat_shipment set shipping_label='' where entity_id='".$track->getId()."'");
				}
			}
        }
        return true;
    }

	protected function _getShipmentItems($shipment_id)
	{
		$db = Mage::getSingleton('core/resource')->getConnection('core_read');
		$result = $db->query('select * from sales_flat_shipment_item where parent_id = '.$shipment_id);
		$rows = $result->fetchAll();
		return $rows;
	}

	protected function _deleteShipmentItems($shipment, $items)
	{
		$shipment_id = $shipment->getId();
		$order_id = $shipment->getOrderId();
		$db = Mage::getSingleton('core/resource')->getConnection('core_write');
		$sql_items = 'delete from sales_flat_shipment_item where parent_id = '.$shipment_id.';';
		$sql_items .= 'delete from sales_flat_shipment_grid where entity_id = '.$shipment_id.';';
		$sql_items .= 'delete from sales_flat_shipment_comment where parent_id = '.$shipment_id.';';
		$sql_items .= 'delete from sales_flat_shipment_track where parent_id = '.$shipment_id.';';
		foreach ($items as $key => $item) {
			$sql_items .= 'update sales_flat_order_item set qty_shipped = qty_shipped - '.$item['qty'].' where item_id = '.$item['order_item_id'].';';
		}
		return $db->query($sql_items);
	}
	
	protected function _toHtml()
	{
		$_html = '<script type="text/javascript">window.location.reload();</script>';
		return $_html;
	}
}
