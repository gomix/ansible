<?php
/**
 * ecommbits.com
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Halcourier
 * @package    Halcourier_Shipping
 * @version    Release: 1.0
 * @author     ecommbits.com (desarrollo@ecommbits.com)
 * @copyright  Copyright (c) 2013 ecommbits.com (http://www.ecommbits.com)
 */

class Halcourier_Shipping_Model_Adminhtml_System_Config_Source_Payment_ActiveMethods
{
    public function toOptionArray()
    {
        return $this->getActivePaymentMethods();
    }

	public function getActivePaymentMethods()
	{
		$payments = Mage::getSingleton('payment/config')->getActiveMethods();
		$methods = array(array('value'=>'', 'label'=>Mage::helper('adminhtml')->__('--Seleccionar--')));
		foreach ($payments as $paymentCode=>$paymentModel)
		{
			$paymentTitle = Mage::getStoreConfig('payment/'.$paymentCode.'/title');
			$methods[$paymentCode] = array(
				'label' => $paymentTitle,
				'value' => $paymentCode,
				);
		}
		return $methods;
	}
}

?>
