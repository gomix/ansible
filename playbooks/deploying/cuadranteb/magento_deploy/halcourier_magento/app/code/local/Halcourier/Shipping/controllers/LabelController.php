<?php
/**
 * ecommbits.com
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Halcourier
 * @package    Halcourier_Shipping
 * @version    Release: 1.0
 * @author     ecommbits.com (desarrollo@ecommbits.com)
 * @copyright  Copyright (c) 2013 ecommbits.com (http://www.ecommbits.com)
 */
 
class Halcourier_Shipping_LabelController extends Mage_Adminhtml_Controller_Sales_Shipment
{
    public function printLabelAction()
    {
        $track_number = $this->getRequest()->getParam('track_number');
        if(isset($track_number) && $track_number != '')
		{
			$shipment = $this->_getShipmentByTrackNumber($track_number);
			if($shipment)
				$this->_printLabel($shipment);
		}			
    }

    protected function _printLabel($shipment = false)
    {
    	if($shipment)
		{
            $pdf = base64_decode($shipment->getShippingLabel());
            $shipment_id = $shipment->getIncrementId();
            header("Content-type: application/pdf");
            header('Content-Disposition: attachment; filename="'.Mage::helper('halcouriershipping')->__('etiqueta_').$shipment_id.'.pdf"');
            echo $pdf;
		}
    }
	
    protected function _getShipmentByTrackNumber($track_number)
    {
        $db = Mage::getSingleton('core/resource')->getConnection('core_read');
		$sql = 'select parent_id from sales_flat_shipment_track where track_number = "'.$track_number.'" and carrier_code = "halcourier"';
        $result = $db->query($sql);
        $row = $result->fetch();
		if (count($row) == 1)
		{
			$shipment = Mage::getModel('sales/order_shipment')->load($row['parent_id']);
			return $shipment;
		}
		else
			return false;
	}
}
