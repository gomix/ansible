<?php
/**
 * ecommbits.com
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Halcourier
 * @package    Halcourier_Shipping
 * @version    Release: 1.0
 * @author     ecommbits.com (desarrollo@ecommbits.com)
 * @copyright  Copyright (c) 2013 ecommbits.com (http://www.ecommbits.com)
 */

class Halcourier_Shipping_Model_Adminhtml_System_Config_Source_Servicemethods
{
    public function toOptionArray()
    {
		$shipping_methods = new Mage_Adminhtml_Model_System_Config_Source_Shipping_Allmethods();
		$shipping_methods = $shipping_methods->toOptionArray(true);

		if(Mage::getConfig()->getModuleConfig('Webshopapps_Matrixrate')->is('active', 'true'))
		{
			$shipping_methods = array_merge($shipping_methods, $this->_getSelectMatrixShipmentMethods());
		}
		
		return $shipping_methods;
    }

	protected function _getSelectMatrixShipmentMethods()
	{
		try
		{
			$arr = array();
			if(Mage::getConfig()->getModuleConfig('Webshopapps_Matrixrate')->is('active', 'true'))
			{
				$title = Mage::getStoreConfig('carriers/matrixrate/title', Mage::app()->getStore()->getStoreId());
				$title_free_shipping = Mage::getStoreConfig('carriers/matrixrate/free_method_text', Mage::app()->getStore()->getStoreId());
				$collection = Mage::getResourceModel('matrixrate_shipping/carrier_matrixrate_collection');
				$collection->getSelect()->group('delivery_type');
				$arr['matrixrate'] = array('label'=>$title);
				foreach ($collection->getData() as $row) {
				    $arr['matrixrate']['value'][] = array('value'=>'[matrixrate]'.$title.' - '.$row['delivery_type'], 'label'=>'[matrixrate] '.$row['delivery_type']);
				}
				$arr['matrixrate']['value'][] = array('value'=>'[matrixrate]matrixrate_matrixrate_free', 'label'=>'[matrixrate] '.$title_free_shipping);
				return $arr;
			}
			else
			{
				return $arr;
			}
		}
		catch(exception $e)
		{
			return $arr;
		}
	}
}