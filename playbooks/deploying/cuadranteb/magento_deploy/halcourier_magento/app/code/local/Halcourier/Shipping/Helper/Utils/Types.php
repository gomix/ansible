<?php
/**
 * ecommbits.com
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Halcourier
 * @package    Halcourier_Shipping
 * @version    Release: 1.0
 * @author     ecommbits.com (desarrollo@ecommbits.com)
 * @copyright  Copyright (c) 2013 ecommbits.com (http://www.ecommbits.com)
 */

class Halcourier_Shipping_Helper_Utils_Types
{
	function __construct() {
	
	}
}

class LoginTypeIn extends Halcourier_Shipping_Helper_Utils_Types
{
	 public $codUsu;
	 public $codAgencia;
	 public $codCliente;
	 public $codDepartamento;
	 public $password;
	 public $tipoValidacion;
}

class LoginTypeOut extends Halcourier_Shipping_Helper_Utils_Types
{
	 public $return;
	 public $returnError;
}

class ListadoAutenticacion extends Halcourier_Shipping_Helper_Utils_Types
{
	 public $servicioWeb;
	 public $procesoServicioWeb;
}

class Permisos extends Halcourier_Shipping_Helper_Utils_Types
{
	 public $altaEnvio = true;
	 public $modificarEnvio = true;
	 public $borrarEnvio = true;
	 public $busquedaEnvio = true;
	 public $altaIncidenciaEnvio = true;
	 public $actuacionesEnvio = true;
	 public $cargosAdicionales = true;
	 public $informeGeneral = true;
	 public $busquedaEtiqueta = true;
	 public $altaRecogida = true;
	 public $busquedaRecogida = true;
	 public $busquedaEnvioConEstados = true;
	 public $busquedaEnvioConEstadoseIncidencias = true;
	 public $altaRecogidaFija = true;
	 public $busquedaRecogidaFija = true;
	 public $altaDireccionHab = true;
	 public $modDireccionHab = true;
	 public $delDireccionHab = true;
	 public $busqDireccionHab = true;
	 public $modificarEstructura = true;
	 public $mostrarEstructura = true;
	 public $busquedaConsolidarEnvios = true;
	 public $consolidarEnvios = true;
	 public $importacionEnvios = true;
	 public $busquedaAuxTipoServ = true;
	 public $busquedaAuxDepartamento = true;
	 public $busquedaAuxCanalizacion = true;
}

class BusquedaEtiquetasTypeIn extends Halcourier_Shipping_Helper_Utils_Types
{
    public $albaran;
    public $agenciaCargo;
    public $agenciaOrigen;
}

class BusquedaEtiquetasTypeOut extends Halcourier_Shipping_Helper_Utils_Types
{
    public $return;
    public $returnError;
}


class RegistrobusquedaAuxiliaresTypeIn extends Halcourier_Shipping_Helper_Utils_Types
{
    public $Autenticacion;
    public $CP;
    public $Campos;
    public $pageSizeSpecified;
    public $pageSpecified;
    public $formatoBusqueda;
    public $tipoBusqueda;
    public $pagesize;
    public $page;
    public $BuscarPor;
}

class RegistrobusquedaAuxiliaresTypeOut extends Halcourier_Shipping_Helper_Utils_Types
{
    public $return;
    public $returnError;
}

class ListadoBusquedaEtiquetas extends Halcourier_Shipping_Helper_Utils_Types
{
    public $tipo;
    public $codificacion;
    public $tamano;
    public $contenido;
}

class AltaEnvioTypeIn extends Halcourier_Shipping_Helper_Utils_Types
{
    public $autenticar;
    public $codPostalDestino;
    public $fecha;
    public $tipoServicio;
    public $portesDebido;
    public $reembolso;
    public $comisionReembolso;
    public $nombreDestino;
    public $viaDestino;
    public $direccionDestino;
    public $numeroDestino;
    public $pisoDestino;
    public $poblacionDestino;
    public $telefonoDestino;
    public $observaciones;
    public $horaMinimaEntrega;
    public $horaMaximaEntrega;
    public $sabado;
    public $anchoOrigen;
    public $altoOrigen;
    public $largoOrigen;
    public $referencia;
    public $mandaSmsOrigen;
    public $movilOrigen;
    public $mandaSmsDestino;
    public $movilDestino;
    public $mandaEmailOrigen;
    public $emailOrigen;
    public $mandaEmailDestino;
    public $emailDestino;
    public $codDepartamento;
    public $peso;
    public $porcentajeReembolso;
    public $valor;
    public $retorno;
    public $paquetes;
    public $documentos;
    public $gestionOrigen;
    public $gestionDestino;
    public $acuse;
    public $numeroSerie;
    public $numeroImei;
    public $nifOrigen;
    public $nifDestino;
    public $tipoMercancia;
    public $numeroFacturaAduana;
    public $importeFacturaAduana;
    public $tipoRuta;
    public $referenciaCliente;
    public $seriePrepago;
    public $numeroPrepago;
}

class Autenticacion extends Halcourier_Shipping_Helper_Utils_Types
{
    public $codUsu;
    public $codAgencia;
    public $codCliente;
    public $codDepartamento;
    public $password;
    public $tipoValidacion;
}

class AltaEnvioTypeOut extends Halcourier_Shipping_Helper_Utils_Types
{
    public $return;
}

class ListadoAltaEnvio extends Halcourier_Shipping_Helper_Utils_Types
{
    public $codError;
    public $descripcionError;
    public $albaran;
    public $tipoServicioCentral;
    public $codBarras;
    public $fechaEntrega;
    public $codAgenciaDestino;
    public $nombreAgenciaDestino;
    public $telefonoAgenciaDestino;
    public $nombreAgenciaOrigen;
}

class BorrarEnvioTypeIn extends Halcourier_Shipping_Helper_Utils_Types
{
    public $autenticar;
    public $albaran;
}

class BusquedaEnviosConEstadoseIncidenciasTypeIn extends Halcourier_Shipping_Helper_Utils_Types
{
    public $autenticar;
    public $fechaInicial;
    public $fechaFinal;
    public $albaran;
    public $agenciaCargo;
    public $agenciaOrigen;
    public $referencia;
    public $campos;
    public $page;
    public $pageSize;
}

class BusquedaEnviosConEstadoseIncidenciasTypeOut extends Halcourier_Shipping_Helper_Utils_Types
{
    public $return;
    public $returnError;
}
?>