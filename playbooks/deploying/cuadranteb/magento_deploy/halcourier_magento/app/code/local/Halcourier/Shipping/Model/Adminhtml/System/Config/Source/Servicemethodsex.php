<?php
/**
 * ecommbits.com
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Halcourier
 * @package    Halcourier_Shipping
 * @version    Release: 1.0
 * @author     ecommbits.com (desarrollo@ecommbits.com)
 * @copyright  Copyright (c) 2013 ecommbits.com (http://www.ecommbits.com)
 */

class Halcourier_Shipping_Model_Adminhtml_System_Config_Source_Servicemethodsex
{
    public function toOptionArray()
    {
    	$shipping_methods = new Halcourier_Shipping_Model_Adminhtml_System_Config_Source_Servicemethods();
		
		return $shipping_methods->toOptionArray();
    }
}