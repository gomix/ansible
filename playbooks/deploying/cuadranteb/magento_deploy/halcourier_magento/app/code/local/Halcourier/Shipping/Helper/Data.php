<?php
/**
 * ecommbits.com
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Halcourier
 * @package    Halcourier_Shipping
 * @version    Release: 1.0
 * @author     ecommbits.com (desarrollo@ecommbits.com)
 * @copyright  Copyright (c) 2013 ecommbits.com (http://www.ecommbits.com)
 */

class Halcourier_Shipping_Helper_Data extends Mage_Core_Helper_Abstract
{
    const LOG_FILE = 'halcourier_shipping.log';

    public function isStoreEnabled($store = null)
    {
        return Mage::getStoreConfigFlag('carriers/halcouriershipping/active', $store);
    }

    public function getServicesMethods($store = null)
    {
        if($store == null)
    	   $store = Mage::app()->getStore()->getStoreId();
    	$jSonMethods = Mage::getStoreConfig('carriers/halcourier_codigos_tipos_servicio/halcourier_tipos_servicio', $store);
    	$arrayDatas = unserialize($jSonMethods);
    	$servicesMethods = array();
    	foreach ($arrayDatas as $key => $arrayData) {
    		foreach ($arrayData as $key => $data) {
    			$servicesMethods[$key][] = $data;
    		}
    	}

        return $servicesMethods;
    }

    public function getServicesMethodsEx($store = null)
    {
        if($store == null)
            $store = Mage::app()->getStore()->getStoreId();
        $jSonMethods = Mage::getStoreConfig('carriers/halcourier_codigos_tipos_servicio/halcourier_tipos_servicio_ex', $store);
        return unserialize($jSonMethods);
    }

    public function getCheckoutComments($order)
    {
        if(Mage::getConfig()->getModuleConfig('GoMage_Checkout')->is('active', 'true'))
        {
            $_comments = $order->getGomageCheckoutCustomerComment();
            $_comments = strip_tags($_comments);
            return preg_replace('/[\n|\r|\n\r]/i',' ',$_comments);
        }
        else
        {
            return '';
        }
    }

    public function log($message, $level = null)
    {
		$message = '[HALCOURIER] - '.$message;
		Mage::log($message, $level, self::LOG_FILE);
    }

    public function getConfigData($field, $store = null)
    {
    	if($store == null)
			$store = Mage::app()->getStore()->getStoreId();
        $path = 'carriers/halcouriershipping/' . $field;
        return Mage::getStoreConfig($path, $store);
    }
	
	public function loadClass($class, $method = '')
	{
		$base = Mage::getModuleDir('', 'Halcourier_Shipping');
		if (file_exists($base.'/Helper/'.$class.'.php'))
		{
			if($method == 'require')
				require_once $base.'/Helper/'.$class.'.php';
			else
    			include_once $base.'/Helper/'.$class.'.php';
			return true;
		}
		else
			return false;
	}

    public function getTrackingUrl($track)
    {
        try
        {
            $halcourier_title = Mage::getStoreConfig('carriers/halcouriershipping/title', $track->getStoreId());
            $halcourier_codigo_agencia = Mage::getStoreConfig('carriers/halcouriershipping/halcourier_codigo_agencia', $track->getStoreId());
            $halcourier_codigo_cliente = Mage::getStoreConfig('carriers/halcouriershipping/halcourier_codigo_cliente', $track->getStoreId());
            $halcourier_url_seguimiento = Mage::getStoreConfig('carriers/halcourier_avanzadas/halcourier_url_seguimiento', $track->getStoreId());
            $url = $halcourier_url_seguimiento."agencia=".$halcourier_codigo_agencia."&cliori=".$halcourier_codigo_cliente."&albaran=".$track->getNumber()."&c=".$track->getNumber();
            //$url = $halcourier_url_seguimiento."agencia=".$halcourier_codigo_agencia."&cliori=".$halcourier_codigo_cliente."&albaran=".$track->getNumber();
            Mage::log($url);
            return $url;
        }
        catch (Exception $e)
        {
            return false;
        }
    }

    public function functionallyEmpty($o)
    {
        if (empty($o)) return true;
        else if (is_numeric($o)) return false;
        else if (is_string($o)) return !strlen(trim($o));
        else if (is_object($o)) return $this->functionallyEmpty((array)$o);

        foreach($o as $element)
            if ($this->functionallyEmpty($element))
                continue;
            else
                return false;

        return true;
    }

    public function isOrderCashOnDelivery(Mage_Sales_Model_Order $order)
    {
        $cod_method = $this->getConfigData('halcourier_cod_method', $order->getStoreId());
        return $order->getPayment()->getMethod() == $cod_method ? true : false;
    }

    public function isBultos1($store = null)
    {
        $checked = Mage::getStoreConfig('carriers/halcourier_opciones_envio/halcourier_bultos_1', $store);
        return $checked == 1 ? true : false;
    }
	
    public function getBultosCalc($store = null)
    {
        return Mage::getStoreConfig('carriers/halcourier_opciones_envio/halcourier_bultos_calculo', $store);
    }

    public function isEmailDest($store = null)
    {
        $checked = Mage::getStoreConfig('carriers/halcourier_opciones_envio/halcourier_email_destino', $store);
        return $checked == 1 ? true : false;
    }

    public function isSMSDest($store = null)
    {
        $checked = Mage::getStoreConfig('carriers/halcourier_opciones_envio/halcourier_sms_destino', $store);
        return $checked == 1 ? true : false;
    }

    public function isEmailOri($store = null)
    {
        $checked = Mage::getStoreConfig('carriers/halcourier_opciones_envio/halcourier_email_origen', $store);
        return $checked == 1 ? true : false;
    }

    public function isSabado($store = null)
    {
        $checked = Mage::getStoreConfig('carriers/halcourier_opciones_envio/halcourier_sabado', $store);
        return $checked == 1 ? true : false;
    }

    public function isPortes($store = null)
    {
        $checked = Mage::getStoreConfig('carriers/halcourier_opciones_envio/halcourier_portes_debidos', $store);
        return $checked == 1 ? true : false;
    }

    public function isRetorno($store = null)
    {
        $checked = Mage::getStoreConfig('carriers/halcourier_opciones_envio/halcourier_retorno', $store);
        return $checked == 1 ? true : false;
    }

    public function isCheckedByDefaultCreateShipment($store = null)
    {
        $checked = $this->getConfigData('halcourier_checked_default_create_shipment', $store);
        return $checked == 1 ? true : false;
    }

    public function parseDecimalValue($value)
    {
        if (!is_numeric($value))
            return false;

        $value = (float) sprintf('%.4F', $value);
        if ($value < 0.0000)
            return false;

        return $value;
    }

    public function parsePercentageValueAsFraction($value)
    {
        if (!is_string($value))
            return false;

        $value = trim($value);
        if (strlen($value) < 2 || substr($value, -1) != '%')
            return false;

        $percentage = $this->parseDecimalValue(substr($value, 0, strlen($value) - 1));
        if ($percentage === false)
            return false;

        return $percentage / 100;
    }
}
