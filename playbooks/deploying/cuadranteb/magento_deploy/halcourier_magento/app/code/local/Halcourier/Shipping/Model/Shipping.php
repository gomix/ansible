<?php
/**
 * ecommbits.com
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Halcourier
 * @package    Halcourier_Shipping
 * @version    Release: 1.0
 * @author     ecommbits.com (desarrollo@ecommbits.com)
 * @copyright  Copyright (c) 2013 ecommbits.com (http://www.ecommbits.com)
 */

class Halcourier_Shipping_Model_Shipping extends Mage_Shipping_Model_Carrier_Abstract
{
    protected $_code = 'halcourier';
    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        return false;
    }

    public function isTrackingAvailable()
    {
		return true;
    }

    public function getTrackingInfo($tracking_number)
    {
        $tracking_result = $this->getTracking($tracking_number);
        if ($tracking_result instanceof Mage_Shipping_Model_Tracking_Result)
            $trackings = $tracking_result->getAllTrackings();
            if ($trackings)
                return $trackings[0];
        elseif (is_string($tracking_result) && !empty($tracking_result))
            return $tracking_result;
        else return false;
    }

    protected function getTracking($tracking_number)
    {
        $halcourier_title = Mage::getStoreConfig('carriers/halcouriershipping/title', $this->getStoreId());
        $halcourier_codigo_agencia = Mage::getStoreConfig('carriers/halcouriershipping/halcourier_codigo_agencia', $this->getStoreId());
        $halcourier_codigo_cliente = Mage::getStoreConfig('carriers/halcouriershipping/halcourier_codigo_cliente', $this->getStoreId());
        $halcourier_url_seguimiento = Mage::getStoreConfig('carriers/halcourier_avanzadas/halcourier_url_seguimiento', $this->getStoreId());
        $url = $halcourier_url_seguimiento."agencia=".$halcourier_codigo_agencia."&cliori=".$halcourier_codigo_cliente."&albaran=".$tracking_number."&c=".$tracking_number;
		//$url = $halcourier_url_seguimiento."&c=".$tracking_number;
        $tracking_result = Mage::getModel('shipping/tracking_result');
		$msg = $this->_getStatusHistoryHtml($tracking_number);
		if($msg)
        	$msg .= '<p><a href="'.$url.'">'.Mage::helper('halcouriershipping')->__('Pulse aquí para más información sobre el seguimiento del envío').'</a></p>';
		else
			$msg = Mage::helper('halcouriershipping')->__('No hay información de seguimiento disponible en este momento.');
        $tracking_status = Mage::getModel('shipping/tracking_result_status');
        $tracking_status->setCarrier($this->_code);
        $tracking_status->setCarrierTitle($halcourier_title);
        $tracking_status->setTracking($tracking_number);
        $tracking_status->addData(array('status'=>$msg));
        $tracking_result->append($tracking_status);

        return $tracking_result;
    }

    protected function _getStatusHistoryHtml($tracking_number)
    {
    	$_html = '';
    	$status = Mage::getModel('halcouriershipping/shipment')->getHalcourierStatus($tracking_number);
		$isAdmin = false;
		$flag = false;

		if(strpos(Mage::helper('core/url')->getCurrentUrl(),'/?___store=') !== false)
			$isAdmin = true;
		if(isset($status['estadosEnvio']) && $status['estadosEnvio'])
		{
			$flag = true;
			$fechaEstadoActual = new DateTime($status['fechaEstadoActual']);
			$_html .= '<p><strong>'.Mage::helper('halcouriershipping')->__('Estado actual:').'</strong> '.utf8_encode($status['descEstadoActual']).' ('.$fechaEstadoActual->format('d-m-Y H:i:s').')<br />';
			$_html .= '<strong>'.Mage::helper('halcouriershipping')->__('Agencia destino:').'</strong> '.utf8_encode($status['nombreAgenciaDestino']).'<br />';
			$_html .= '<strong>'.Mage::helper('halcouriershipping')->__('Paquetes:').'</strong> '.utf8_encode($status['paquetes']);
			
			if(strpos($status['observaciones'], '[*REC*]') !== false)
				$_html .= '<br /><strong>'.Mage::helper('halcouriershipping')->__('Observaciones:').'</strong> '.Mage::helper('halcouriershipping')->__('Recoger en delegación.');
			$_html .= '</p><p><table><tr><td>'.Mage::helper('halcouriershipping')->__('Fecha').'</td><td>'.Mage::helper('halcouriershipping')->__('Estado').'</td></tr>';
			$status_history = $status['estadosEnvio'];
			if(count($status_history) > 1 && !isset($status_history['fechaEstado']))
			{
				foreach ($status['estadosEnvio'] as $state)
				{
					$fechaEstado = new DateTime($state['fechaEstado']);
					$_html .= '<tr><td>'.$fechaEstado->format('d-m-Y H:i:s').'</td><td>'.utf8_encode($state['descripcionEstado']).'</td></tr>';
				}
			}
			else
			{
				$fechaEstado = new DateTime($status_history['fechaEstado']);
				$_html .= '<tr><td>'.$fechaEstado->format('d-m-Y H:i:s').'</td><td>'.utf8_encode($status_history['descripcionEstado']).'</td></tr>';
			}
			$_html .= '</table></p>';
		}
		else
		{
			$flag = false;
		}

		if(isset($status['incidenciasEnvio']) && $status['incidenciasEnvio'])
		{
			$flag = true;
			if($isAdmin)
			{
				$_html .= '<p><strong>'.Mage::helper('halcouriershipping')->__('Incidencias:').'</strong></p>';
				$inc_history = $status['incidenciasEnvio'];
				$_html .= '<p><table><tr><td>'.Mage::helper('halcouriershipping')->__('Fecha').'</td><td>'.Mage::helper('halcouriershipping')->__('Estado').'</td></tr>';
				if(count($inc_history) > 1 && !isset($inc_history['fechaIncidencia']))
				{
					foreach ($status['incidenciasEnvio'] as $state)
					{
						$fechaEstado = new DateTime($state['fechaIncidencia']);
						$_html .= '<tr><td>'.$fechaEstado->format('d-m-Y H:i:s').'</td><td>'.utf8_encode($state['descripcionIncidencia']).'</td></tr>';
					}
				}
				else
				{
					$fechaEstado = new DateTime($inc_history['fechaIncidencia']);
					$_html .= '<tr><td>'.$fechaEstado->format('d-m-Y H:i:s').'</td><td>'.utf8_encode($inc_history['descripcionIncidencia']).'</td></tr>';
				}
				$_html .= '</table></p>';
			}
		}

		if($flag)
			return $_html;
		else
			return false;
    }
}
