<?php
/**
 * ecommbits.com
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Halcourier
 * @package    Halcourier_Shipping
 * @version    Release: 1.0
 * @author     ecommbits.com (desarrollo@ecommbits.com)
 * @copyright  Copyright (c) 2013 ecommbits.com (http://www.ecommbits.com)
 */

class Halcourier_Shipping_Model_Shipment extends Mage_Sales_Model_Order_Shipment
{
    protected function _beforeSave()
    {
    	$data = Mage::app()->getRequest()->getParams();
        $canSave = parent::_beforeSave();
		$log = Mage::helper('halcouriershipping')->getConfigData('log');
        $order_id = $this->getOrder()->getId();
        $order_increment_id = $this->getOrder()->getIncrementId();

		if(!isset($data['halcouriershipping_communicate_shipment']) || !$data['halcouriershipping_communicate_shipment'])
		{
			if($log)
				Mage::helper('halcouriershipping')->log('[ENVIO NO PROCESADO POR HALCOURIER] PEDIDO #'.$order_increment_id,5);
			return $canSave;
		}

		$shipping_method = $this->getOrder()->getShippingMethod();
		if(strpos($shipping_method, 'matrixrate_matrixrate') !== false)
			$shipping_method = '[matrixrate]'.$this->getOrder()->getShippingDescription();
		
		if(Mage::helper('halcouriershipping')->getConfigData('halcourier_pickup_method') == $shipping_method)
		{
			if($log)
				Mage::helper('halcouriershipping')->log('[ENVIO NO PROCESADO POR HALCOURIER][ENTREGA EN TIENDA] PEDIDO #'.$order_increment_id,5);
			return $canSave;
		}

		$this->_checkURLWS();
		
		if ($this->_existsShipments($order_id))
		{
			$message = Mage::helper('halcouriershipping')->__('Ya existe un envío activo gestionado por Halcourier. Debe cancelarlo primero.');
			if($log)
				Mage::helper('halcouriershipping')->log('[YA EXISTE UN ENVIO GESTIONADO POR HALCOURIER] PEDIDO #'.$order_increment_id,5);
			Mage::throwException(Mage::helper('halcouriershipping')->__('Halcourier error:').' '.$message);
		}
		else
		{
			if(!Mage::helper('halcouriershipping')->loadClass('Soap/nusoap','require') ||
				!Mage::helper('halcouriershipping')->loadClass('Utils/Types') ||
				!Mage::helper('halcouriershipping')->loadClass('Utils'))
			{
				Mage::helper('halcouriershipping')->log('[ALTA ENVIO][ERROR EN LA CARGA DE CLASES] PEDIDO #'.$order_increment_id,9);
			}

            $dir_entrega = $this->getShippingAddress();

            //Comprobar la validez del código postal
            $countryId = $dir_entrega->getCountryId();
            if ($countryId == 'ES' || $countryId == 'PT') {
                $result = $this->_checkZipCode($dir_entrega->getPostcode(), $countryId, $log);
                if (!is_object($result) || Mage::helper('halcouriershipping')->functionallyEmpty($result->RegistrobusquedaAuxiliaresResult->Result)) {
                	if ($result->RegistrobusquedaAuxiliaresResult->ErrorHlcbusqueda) {
						$error_message = '[99998] '.$result->RegistrobusquedaAuxiliaresResult->ErrorHlcbusqueda;
                	} else {
						$error_message = '[99999] '. Mage::helper('halcouriershipping')->__('Código postal no válido');
                	}
                    Mage::helper('halcouriershipping')->log('[ALTA ENVIO - VALIDACION CP] Halcourier error: '.print_r($error_message,true),5);
                    Mage::throwException(Mage::helper('halcouriershipping')->__('Halcourier error:').' '.$error_message);
                }
            }
			
			$_items = $this->getAllItems();
			$halcourier_peso_origen = 0;
			foreach ($_items as $_item)
				if($_item->getOrderItem()->getWeight())
					$halcourier_peso_origen += $_item->getOrderItem()->getWeight() * $_item->getOrderItem()->getQtyToShip();
				else
					$halcourier_peso_origen += 0;
			if($halcourier_peso_origen < 1)
			    $halcourier_peso_origen = 1;
			$halcourier_bultos = $data['halcouriershipping_bultos'];

			$halcourier_referencia = strip_tags($data['halcouriershipping_referencia']);

			$halcourier_importe_servicio 		  = $this->getOrder()->getGrandTotal();
			$halcourier_nombre_destinatario       = $this->cleanStr($dir_entrega->getName());
			$halcourier_nombre_via_destinatario   = preg_replace('/[\n|\r|\n\r]/i', ' ', $this->cleanStr($dir_entrega->getStreetFull()));
			$halcourier_poblacion_destinatario    = $this->cleanStr($dir_entrega->getCity());
			$halcourier_CP_destinatario           = $dir_entrega->getPostcode();
			$halcourier_pais_destinatario         = $dir_entrega->getCountryId();
			$halcourier_telefono_destinatario     = $dir_entrega->getTelephone();
			$halcourier_CP_remitente              = Mage::helper('halcouriershipping')->getConfigData('halcourier_codigo_postal', $this->getOrder()->getStoreId());
			$halcourier_nif_destino				  = $dir_entrega->getTaxvat();
			$halcourier_nif_origen				  = Mage::getStoreConfig('general/store_information/merchant_vat_number');
			$halcourier_nombre_pais_dest		  = $this->cleanStr(Mage::app()->getLocale()->getCountryTranslation($halcourier_pais_destinatario));

			if($halcourier_CP_remitente == '')
				$halcourier_CP_remitente = Mage::getStoreConfig('shipping/origin/postcode');
			$codAgencia = Mage::helper('halcouriershipping')->getConfigData('halcourier_codigo_agencia', $this->getOrder()->getStoreId());
			$codCliente = Mage::helper('halcouriershipping')->getConfigData('halcourier_codigo_cliente', $this->getOrder()->getStoreId());
            $codUsu = Mage::helper('halcouriershipping')->getConfigData('halcourier_codigo_usuario', $this->getOrder()->getStoreId());
			$password = Mage::helper('core')->decrypt(Mage::helper('halcouriershipping')->getConfigData('halcourier_password_cliente', $this->getOrder()->getStoreId()));
			$observaciones = '';
			
			if(isset($data['halcouriershipping_cod']) && $data['halcouriershipping_cod'] == '1')
			{
			    $halcourier_reembolso = floatval($data['cod_amount']);
				$halcourier_comisionreembolso = iconv("ISO-8859-1","UTF-8", 'S');
				//$halcourier_porcentajereembolso = number_format(1 - floatval($data['cod_amount'] / $halcourier_importe_servicio), 2);
			} else {
			    $halcourier_reembolso = '';
				$halcourier_comisionreembolso = iconv("ISO-8859-1","UTF-8", 'N');
				//$halcourier_porcentajereembolso = '';
			}
			
			if(isset($data['halcouriershipping_sabado']) && $data['halcouriershipping_sabado'] == '1')
				$halcourier_sabado = iconv("ISO-8859-1","UTF-8", 'S');
			else
				$halcourier_sabado = iconv("ISO-8859-1","UTF-8", 'N');

			if(isset($data['halcouriershipping_portes_debidos']) && $data['halcouriershipping_portes_debidos'] == '1')
				$halcouriershipping_portes_debidos = iconv("ISO-8859-1","UTF-8", 'S');
			else
				$halcouriershipping_portes_debidos = iconv("ISO-8859-1","UTF-8", 'N');

			if(isset($data['halcouriershipping_retorno']) && $data['halcouriershipping_retorno'] == '1')
				$halcouriershipping_retorno = iconv("ISO-8859-1","UTF-8", 'S');
			else
				$halcouriershipping_retorno = iconv("ISO-8859-1","UTF-8", 'N');
			
			if(isset($data['halcouriershipping_sms']) && $data['halcouriershipping_sms'] == '1')
			{
				$halcourier_sms = iconv("ISO-8859-1","UTF-8", 'S');
				$halcourier_sms_phone = $data['halcouriershipping_sms_telf'];
			}
			else
			{
				$halcourier_sms = iconv("ISO-8859-1","UTF-8", 'N');
				$halcourier_sms_phone= '';
			}

			if(isset($data['halcouriershipping_email']) && $data['halcouriershipping_email'] == '1')
			{
				$halcourier_email = iconv("ISO-8859-1","UTF-8", 'S');
				$halcourier_email_txt = $data['halcouriershipping_email_txt'];
			}
			else
			{
				$halcourier_email = iconv("ISO-8859-1","UTF-8", 'N');
				$halcourier_email_txt = '';
			}

			if(isset($data['halcouriershipping_email_dest']) && $data['halcouriershipping_email_dest'] == '1')
			{
				$halcourier_email_dest = iconv("ISO-8859-1","UTF-8", 'S');
				$halcourier_email_dest_txt = $data['halcouriershipping_email_dest_txt'];
			}
			else
			{
				$halcourier_email_dest = iconv("ISO-8859-1","UTF-8", 'N');
				$halcourier_email_dest_txt = '';
			}

			$tiposervicio = $data['halcouriershipping_servicio'];
			$tiposervicio_msg = '';
			if (substr($halcourier_CP_destinatario,0,2) == '35' || substr($halcourier_CP_destinatario,0,2) == '38') //CANARIAS
			{
				$tiposervicio = Mage::getStoreConfig('carriers/halcourier_codigos_tipos_servicio/halcourier_codcanarias', $this->getOrder()->getStoreId());
				$tiposervicio_msg = Mage::helper('halcouriershipping')->__('Envío a Canarias.');
			}
			if(substr($halcourier_CP_destinatario,0,2) == '07') //BALEARES
			{
				$tiposervicio = Mage::getStoreConfig('carriers/halcourier_codigos_tipos_servicio/halcourier_codbaleares', $this->getOrder()->getStoreId());
				$tiposervicio_msg = Mage::helper('halcouriershipping')->__('Envío a Baleares.');
			}
			if(substr($halcourier_CP_destinatario,0,2) == '51') //CEUTA
			{
				$tiposervicio = Mage::getStoreConfig('carriers/halcourier_codigos_tipos_servicio/halcourier_codceuta', $this->getOrder()->getStoreId());
				$tiposervicio_msg = Mage::helper('halcouriershipping')->__('Envío a Ceuta.');
			}	
			if(substr($halcourier_CP_destinatario,0,2) == '52') //MELILLA
			{
				$tiposervicio = Mage::getStoreConfig('carriers/halcourier_codigos_tipos_servicio/halcourier_codmelilla', $this->getOrder()->getStoreId());
				$tiposervicio_msg = Mage::helper('halcouriershipping')->__('Envío a Melilla.');
			}	
			if($halcourier_pais_destinatario == 'AD') //ANDORRA
			{
				$tiposervicio = Mage::getStoreConfig('carriers/halcourier_codigos_tipos_servicio/halcourier_codandorra', $this->getOrder()->getStoreId());
				$tiposervicio_msg = Mage::helper('halcouriershipping')->__('Envío a Andorra.');
			}
			if($halcourier_pais_destinatario == 'GI') //GIBRALTAR
			{
				$tiposervicio = Mage::getStoreConfig('carriers/halcourier_codigos_tipos_servicio/halcourier_codgibraltar', $this->getOrder()->getStoreId());
				$tiposervicio_msg = Mage::helper('halcouriershipping')->__('Envío a Gibraltar.');
			}
			if($halcourier_pais_destinatario != 'ES' && $halcourier_pais_destinatario != 'AD' && $halcourier_pais_destinatario != 'GI' && $halcourier_pais_destinatario != 'PT') //INTERNACIONAL EXCEPTO PORTUGAL
			{
				//if($tiposervicio == '')
                    $tiposervicio = Mage::getStoreConfig('carriers/halcourier_codigos_tipos_servicio/halcourier_codinternacional', $this->getOrder()->getStoreId());
				$observaciones .= Mage::helper('halcouriershipping')->__('[').$halcourier_nombre_pais_dest.' (CP '.$halcourier_CP_destinatario.')'.Mage::helper('halcouriershipping')->__(']');
				$halcourier_CP_destinatario = '0';
				$tiposervicio_msg = Mage::helper('halcouriershipping')->__('Envío Internacional.');					
			}
			if($halcourier_pais_destinatario == 'PT') //PORTUGAL
			{
				//if($tiposervicio == '')
					$tiposervicio = Mage::getStoreConfig('carriers/halcourier_codigos_tipos_servicio/halcourier_codinternacional', $this->getOrder()->getStoreId());
				$observaciones .= Mage::helper('halcouriershipping')->__('[').$halcourier_nombre_pais_dest.' (CP '.$halcourier_CP_destinatario.')'.Mage::helper('halcouriershipping')->__(']');
				$halcourier_CP_destinatario = substr($halcourier_CP_destinatario, 0, 4);
				$tiposervicio_msg = Mage::helper('halcouriershipping')->__('Envío a Portugal.');	
			}
			$login = new LoginTypeIn();
			$permisos = new Permisos();
			$permisos->altaEnvio = true;
			$permisos->busquedaEtiqueta = true;
            $login->codUsu = $codUsu;
			$login->codAgencia = $codAgencia;
			$login->codCliente = $codCliente;
			$login->password = $password;
			$login->tipoValidacion = 'Cliente';
			$client = new nusoap_client(Mage::getStoreConfig('carriers/halcourier_avanzadas/halcourier_url_envios', $this->getOrder()->getStoreId()),true);
			$envio = new AltaEnvioTypeIn();
			$envio->autenticar = Halcourier_Shipping_Helper_Utils::objectToArray($login);
			$envio->codPostalDestino = substr($halcourier_CP_destinatario, 0, 5);
			$envio->fecha = date('c');
			$envio->tipoServicio = $tiposervicio;
			$envio->nombreDestino = strtoupper(substr($halcourier_nombre_destinatario, 0, 39));
			$envio->direccionDestino = strtoupper(substr($halcourier_nombre_via_destinatario, 0, 49));
			$envio->poblacionDestino = strtoupper(substr($halcourier_poblacion_destinatario, 0, 59));
			$envio->telefonoDestino = iconv("ISO-8859-1","UTF-8", substr($halcourier_telefono_destinatario, 0, 15));
			$envio->referencia = substr($halcourier_referencia, 0, 30);
			$envio->referenciaCliente = substr($halcourier_referencia, 0, 30);
			$envio->peso = iconv("ISO-8859-1","UTF-8", floatval($halcourier_peso_origen));
			$envio->reembolso = iconv("ISO-8859-1","UTF-8", $halcourier_reembolso);
			$envio->comisionReembolso = $halcourier_comisionreembolso;
			//$envio->porcentajeReembolso = $halcourier_porcentajereembolso;
			$envio->nifOrigen = $halcourier_nif_origen;
			$envio->nifDestino = $halcourier_nif_destino;
			$envio->paquetes = iconv("ISO-8859-1","UTF-8", $halcourier_bultos);
			if(isset($data['halcouriershipping_recogida_delegacion']) && $data['halcouriershipping_recogida_delegacion'] == '1')
				$observaciones .= Mage::helper('halcouriershipping')->__('[*REC*][RECOGIDA EN DELEGACIÓN]').' ';
			$envio->observaciones = strtoupper(substr(Halcourier_Shipping_Helper_Utils::cleanString($observaciones.strip_tags($data['halcouriershipping_observaciones'])), 0, 250));
			$envio->mandaEmailOrigen = $halcourier_email;
			$envio->emailOrigen = strip_tags($halcourier_email_txt);
			$envio->mandaEmailDestino = $halcourier_email_dest;
			$envio->emailDestino = strip_tags($halcourier_email_dest_txt);
			$envio->mandaSmsDestino = $halcourier_sms;
			$envio->movilDestino = $halcourier_sms_phone;
			$envio->sabado = $halcourier_sabado;
			$envio->portesDebido = $halcouriershipping_portes_debidos;
			$envio->retorno = $halcouriershipping_retorno;
			$body = Halcourier_Shipping_Helper_Utils::objectsToNusoap($envio->autenticar, $envio, 'AltaEnvioTypeIn');
			$client->soap_defencoding = 'UTF-8';
			$client->useHTTPPersistentConnection();
			$client->setUseCurl(true);
			$response = $client->send($body, 'AltaEnvio');
			if($log)
			{
				Mage::helper('halcouriershipping')->log('[ALTA ENVIO] '.print_r($envio,true),5);
				Mage::helper('halcouriershipping')->log('[RESPUESTA ALTA ENVIO] '.print_r($response,true),5);
			}
			$errors = false;
			if(isset($response['return']) && is_array($response['return']))
			{
				if ($response['return']['codError'] != '0')
				{
					$error_message = '['.$response['return']['codError'].'] '.iconv("ISO-8859-1","UTF-8", $response['return']['descripcionError']);
					$errors = true;
					$response['return']['albaran'] = '';
				}
			}
			else
			{
				if(isset($response['faultstring']))
					$error_message = '[99999] '.iconv("ISO-8859-1","UTF-8", $response['faultstring']);
				else
					$error_message = '[99999] '.Mage::helper('halcouriershipping')->__('Error indeterminado. Compruebe la conexión con Halcourier o vuelva a intentarlo pasados unos minutos.');
				$errors = true;
			}
			if ($errors == true)
			{
				if($tiposervicio_msg != '')
					$error_message .= ' '.$tiposervicio_msg.' '.Mage::helper('halcouriershipping')->__('Compruebe la configuración.');
				Mage::log('Halcourier error: '.$error_message);
			    Mage::throwException(Mage::helper('halcouriershipping')->__('Halcourier error:').' '.$error_message);
			}
			else 
			{
				$halcourier_num_albaran = $response['return']['albaran'];
				$track = Mage::getModel('sales/order_shipment_track')
			            ->setNumber($order_id)
			            ->setCarrierCode('halcourier')
			            ->setTitle(Mage::helper('halcouriershipping')->__('Halcourier'))
			            ->setDescription(Mage::helper('halcouriershipping')->__('Referencia:').' '.$halcourier_referencia)
						->setQty($halcourier_bultos)
						->setWeight($halcourier_peso_origen)
						->setTrack_number($halcourier_num_albaran);
				$this->addTrack($track);
			}

			$login = new LoginTypeIn();
            $login->codUsu = $codUsu;
			$login->codAgencia = $codAgencia;
			$login->codCliente = $codCliente;
			$login->password = $password;
			$login->tipoValidacion = 'Cliente';
			$client = new nusoap_client(Mage::getStoreConfig('carriers/halcourier_avanzadas/halcourier_url_etiquetas', $this->getOrder()->getStoreId()),true);
			$envio = new BusquedaEtiquetasTypeIn();
			$envio->autenticar = Halcourier_Shipping_Helper_Utils::objectToArray($login);
			$envio->albaran = iconv("ISO-8859-1","UTF-8", $halcourier_num_albaran);
			$envio->agenciaCargo = iconv("ISO-8859-1","UTF-8", $codAgencia);
			$envio->agenciaOrigen = iconv("ISO-8859-1","UTF-8", $codAgencia);
			$body = Halcourier_Shipping_Helper_Utils::objectsToNusoap($envio->autenticar, $envio, 'BusquedaEtiquetasTypeIn');
			$client->soap_defencoding = 'ISO-8859-1';
			$client->useHTTPPersistentConnection();
			$client->setUseCurl(true);
			$response = $client->send($body, 'BusquedaEtiquetas');
			if($log)
			{
				Mage::helper('halcouriershipping')->log('[ETIQUETA] '.print_r($envio,true),5);
				Mage::helper('halcouriershipping')->log('[RESPUESTA ETIQUETA] OK - '.print_r($response['return']['tipo'],true),5);
			}
			$this->setShippingLabel($response['return']['contenido']);
			Mage::getSingleton('adminhtml/session')->addSuccess(
						Mage::helper('halcouriershipping')->__('Halcourier: Número de albarán generado:').' '.$halcourier_num_albaran.' '.$this->getPrintLabelButtonHtml($halcourier_num_albaran));
		}
        return $canSave;
    }

    protected function _beforeDelete()
    {
		$canDelete = parent::_beforeDelete();
		$log = Mage::helper('halcouriershipping')->getConfigData('log');
		foreach ($this->getAllTracks() as $track)
		{
			if ($track->getCarrierCode() == 'halcourier')
			{
				$ordenId = $track->getOrderId();
				if(!Mage::helper('halcouriershipping')->loadClass('Soap/nusoap','require') ||
					!Mage::helper('halcouriershipping')->loadClass('Utils/Types') ||
					!Mage::helper('halcouriershipping')->loadClass('Utils'))
				{
					Mage::helper('halcouriershipping')->log('[BORRAR ENVIO][ERROR EN LA CARGA DE CLASES] PEDIDO #'.$ordenId,9);
				}
				$db = Mage::getSingleton('core/resource')->getConnection('core_write');
				$result = $db->query("select track_number from sales_flat_shipment_track where entity_id = '".$track->getId()."' and carrier_code = 'halcourier' limit 1");
				$row = $result->fetch();
				if(count($row) == 0)
					return $canDelete;
				$login = new LoginTypeIn();
                $login->codUsu = Mage::helper('halcouriershipping')->getConfigData('halcourier_codigo_usuario', $this->getOrder()->getStoreId());
				$login->codAgencia = Mage::helper('halcouriershipping')->getConfigData('halcourier_codigo_agencia', $this->getOrder()->getStoreId());
				$login->codCliente = Mage::helper('halcouriershipping')->getConfigData('halcourier_codigo_cliente', $this->getOrder()->getStoreId());
				$login->password = Mage::helper('core')->decrypt(Mage::helper('halcouriershipping')->getConfigData('halcourier_password_cliente', $this->getOrder()->getStoreId()));
				$login->tipoValidacion = 'Cliente';
				$client = new nusoap_client(Mage::getStoreConfig('carriers/halcourier_avanzadas/halcourier_url_envios', $this->getOrder()->getStoreId()),true);
				$envio = new BorrarEnvioTypeIn();
				$envio->autenticar = Halcourier_Shipping_Helper_Utils::objectToArray($login);
				$envio->albaran = $row['track_number'];
				$body = Halcourier_Shipping_Helper_Utils::objectsToNusoap($envio->autenticar,$envio,'BorrarEnvioTypeIn');
				$client->soap_defencoding = 'utf-8';
				$client->useHTTPPersistentConnection();
				$client->setUseCurl(true);
				$response = $client->send($body, 'BorrarEnvio');
				if($log)
				{
					Mage::helper('halcouriershipping')->log('[BORRAR ENVIO] '.print_r($envio,true),5);
					Mage::helper('halcouriershipping')->log('[RESPUESTA BORRAR ENVIO] '.print_r($response,true),5);
				}
				$errors = false;
				if(isset($response['return']) && is_array($response['return']))
				{
					if ($response['return']['codError'] != '0' && $response['return']['codError'] != '2')
					{
						$error_message = '['.$response['return']['codError'].'] '.$response['return']['descripcionError'];
						$errors = true;
					}
					else
					{
						Mage::getSingleton('adminhtml/session')->addSuccess(
							Mage::helper('halcouriershipping')->__('Halcourier: Albarán').' '.$row['track_number'].' '.Mage::helper('halcouriershipping')->__('eliminado correctamente.'));
					}
					if ($response['return']['codError'] == '2')
					{
						Mage::getSingleton('adminhtml/session')->getMessages(true);
						$errors = false;
						Mage::getSingleton('adminhtml/session')->addSuccess(
							Mage::helper('halcouriershipping')->__('Halcourier: El albarán').' '.$row['track_number'].' '.Mage::helper('halcouriershipping')->__('ya estaba eliminado en el sistema.'));
					}
				}
				else
				{
					if(isset($response['faultstring']))
						$error_message = '[99999] '.iconv("ISO-8859-1","UTF-8", $response['faultstring']);
					else
						$error_message = '[99999] '.Mage::helper('halcouriershipping')->__('Error indeterminado. Compruebe la conexión con Halcourier o vuelva a intentarlo pasados unos minutos.');
					$errors = true;
				}
				if ($errors == true)
				{
					Mage::log('Halcourier error: '.$error_message);
					Mage::throwException(Mage::helper('halcouriershipping')->__('Halcourier error:').' '.$error_message);
				}
				else
				{
					$result = $db->query("update sales_flat_shipment set shipping_label='' where entity_id='".$track->getId()."'");
				}
			}
        }
        return $canDelete;
	}

    protected function _existsShipments($order_id)
    {
    	if(Mage::helper('halcouriershipping')->getConfigData('halcourier_envios_multiples', $this->getOrder()->getStoreId()) == '1')
			return false;
        $db = Mage::getSingleton('core/resource')->getConnection('core_read');
		$sql = 'select track_number from sales_flat_shipment_track where order_id = '.$order_id.' and carrier_code = "halcourier"';
        $result = $db->query($sql);
        $row = $result->fetchAll();

		if (count($row) > 0)
			return true;
		else
			return false;
	}

    protected function _checkURLWS()
    {
    	try
    	{
	    	$flag = false;
			$store_config = new Mage_Core_Model_Config();
			$halcourier_url_envios = 'https://185.11.117.135/halsystemSW/services/MantenimientoEnvios?wsdl';
			$halcourier_url_etiquetas = 'https://185.11.117.135/halsystemSW/services/MantenimientoEtiquetas?wsdl';
			$halcourier_url_cp = 'http://213.27.170.171/wsHLCBusquedaAuxiliares20/wsHLCBusquedaAuxiliares.svc?singleWsdl';
			//$halcourier_url_seguimiento = 'http://www.halcourier.es/consultaweb/ver_localiza_albaran.asp?';
			$halcourier_url_seguimiento = 'http://www.halcourier.es/_layouts/utilidades/LocalizarEnvio.aspx?';
			$halcourier_estados_finales = 'ENB,ENT,ENTD,ENTI,ENTM,ESC,FBB';

	    	if(Mage::getStoreConfig('carriers/halcourier_avanzadas/halcourier_url_envios', 0) == '')
			{
				$store_config ->saveConfig('carriers/halcourier_avanzadas/halcourier_url_envios', $halcourier_url_envios, 'default', 0);
				$flag = true;
			}
			
	    	if(Mage::getStoreConfig('carriers/halcourier_avanzadas/halcourier_url_etiquetas', 0) == '')
			{
				$store_config ->saveConfig('carriers/halcourier_avanzadas/halcourier_url_etiquetas', $halcourier_url_etiquetas, 'default', 0);
				$flag = true;
			}
            
            if(Mage::getStoreConfig('carriers/halcourier_avanzadas/halcourier_url_cp', 0) == '')
            {
                $store_config ->saveConfig('carriers/halcourier_avanzadas/halcourier_url_cp', $halcourier_url_cp, 'default', 0);
                $flag = true;
            }
			
	    	if(Mage::getStoreConfig('carriers/halcourier_avanzadas/halcourier_url_seguimiento', 0) == '')
			{
				$store_config ->saveConfig('carriers/halcourier_avanzadas/halcourier_url_seguimiento', $halcourier_url_seguimiento, 'default', 0);
				$flag = true;
			}
			
	    	if(Mage::getStoreConfig('carriers/halcourier_avanzadas/halcourier_estados_finales', 0) == '')
			{
				$store_config ->saveConfig('carriers/halcourier_avanzadas/halcourier_estados_finales', $halcourier_estados_finales, 'default', 0);
				$flag = true;
			}

	    	if(Mage::getStoreConfig('carriers/halcourier_avanzadas/halcourier_url_envios', $this->getOrder()->getStoreId()) == '')
			{
				$store_config ->saveConfig('carriers/halcourier_avanzadas/halcourier_url_envios', $halcourier_url_envios, 'stores', $this->getOrder()->getStoreId());
				$flag = true;
			}
			
	    	if(Mage::getStoreConfig('carriers/halcourier_avanzadas/halcourier_url_etiquetas', $this->getOrder()->getStoreId()) == '')
			{
				$store_config ->saveConfig('carriers/halcourier_avanzadas/halcourier_url_etiquetas', $halcourier_url_etiquetas, 'stores', $this->getOrder()->getStoreId());
				$flag = true;
			}

            if(Mage::getStoreConfig('carriers/halcourier_avanzadas/halcourier_url_cp', $this->getOrder()->getStoreId()) == '')
            {
                $store_config ->saveConfig('carriers/halcourier_avanzadas/halcourier_url_cp', $halcourier_url_cp, 'stores', $this->getOrder()->getStoreId());
                $flag = true;
            }
			
	    	if(Mage::getStoreConfig('carriers/halcourier_avanzadas/halcourier_url_seguimiento', $this->getOrder()->getStoreId()) == '')
			{
				$store_config ->saveConfig('carriers/halcourier_avanzadas/halcourier_url_seguimiento', $halcourier_url_seguimiento, 'stores', $this->getOrder()->getStoreId());
				$flag = true;
			}

	    	if(Mage::getStoreConfig('carriers/halcourier_avanzadas/halcourier_estados_finales', $this->getOrder()->getStoreId()) == '')
			{
				$store_config ->saveConfig('carriers/halcourier_avanzadas/halcourier_estados_finales', $halcourier_estados_finales, 'stores', $this->getOrder()->getStoreId());
				$flag = true;
			}
	
			if($flag)
			{
				Mage::getConfig()->reinit();
				Mage::app()->reinitStores();			
			}
			return true;
    	}
		catch (Exception $e)
		{
			$error_message = Mage::helper('halcouriershipping')->__('Halcourier error:').' '.Mage::helper('halcouriershipping')->__('Error en la configuración. Faltan definir las URL de comunicación con Halcourier.');
			Mage::log($error_message);
			Mage::throwException($error_message);
		}
	}

    protected function _checkZipCode($postcode, $countryId, $log)
    {
        $login = new LoginTypeIn();
        $login->codUsu = Mage::helper('halcouriershipping')->getConfigData('halcourier_codigo_usuario', $this->getOrder()->getStoreId());
        $login->codAgencia = Mage::helper('halcouriershipping')->getConfigData('halcourier_codigo_agencia', $this->getOrder()->getStoreId());
        $login->codCliente = Mage::helper('halcouriershipping')->getConfigData('halcourier_codigo_cliente', $this->getOrder()->getStoreId());
        $login->password = Mage::helper('core')->decrypt(Mage::helper('halcouriershipping')->getConfigData('halcourier_password_cliente', $this->getOrder()->getStoreId()));
        $login->tipoValidacion = 'Cliente';

        $client = new SoapClient(Mage::getStoreConfig('carriers/halcourier_avanzadas/halcourier_url_cp', $this->getOrder()->getStoreId()), array('trace' => true));

        $buscarCP = new RegistrobusquedaAuxiliaresTypeIn();
        $buscarCP->Autenticacion = Halcourier_Shipping_Helper_Utils::objectToArray($login);

        if ($countryId == 'PT')
            $buscarCP->CP = substr($postcode, 0, 4);
        else
            $buscarCP->CP = $postcode;

        $buscarCP->Campos = '*';
        $buscarCP->pageSizeSpecified = 'false';
        $buscarCP->pageSpecified = 'false';
        $buscarCP->formatoBusqueda = 'true';
        $buscarCP->tipoBusqueda = 'true';
        $buscarCP->page = 1;
        $buscarCP->pagesize = 1;
        $buscarCP->BuscarPor = 'CodigoPostal';
        $body = Halcourier_Shipping_Helper_Utils::objectsToNusoapBusquedasAuxiliares($buscarCP->Autenticacion, $buscarCP, 'RegistrobusquedaAuxiliares');
        $body = new SoapVar($body, XSD_ANYXML);
        
        $response = $client->RegistrobusquedaAuxiliares($body);

        if($log)
        {
            Mage::helper('halcouriershipping')->log('[BUSCARCP] '.print_r($buscarCP,true),5);
            Mage::helper('halcouriershipping')->log('[BUSCARCP_BODY] '.print_r($body,true),5);
            Mage::helper('halcouriershipping')->log('[RESPUESTA BUSCARCP] OK - '.print_r($response,true),5);
        }

        return $response;
    }

    public static function _getPreviousTracks($order_id)
    {
        $db = Mage::getSingleton('core/resource')->getConnection('core_read');
		$sql = 'select track_number from sales_flat_shipment_track where order_id = '.$order_id.' and carrier_code = "halcourier"';
        $result = $db->query($sql);
        $row = $result->fetchAll();
		if (count($row) > 0)
			return count($row);
		else
			return 0;
	}

    public function getRemoveUrl($track, $shipment)
    {
		return Mage::getUrl('halcouriershipping/shipment/removeTrack/', array(
			'shipment_id' => $shipment->getId(),
			'track_id' => $track->getId()
		));
    }
	
    public function getPrintLabelButtonHtml($track_number)
    {
    	$html = '';
		$html = '<button title="Etiqueta Halcourier" type="button" class="scalable go"';
		$html .= 'onclick="window.open(\''.$this->getLabelHalcourierUrl($track_number).'\')"><span><span><span>'.Mage::helper('halcouriershipping')->__('Generar etiqueta').'</span></span></span></button>';
		return $html;
    }

    public function getLabelHalcourierUrl($track_number)
    {
    	return Mage::helper("adminhtml")->getUrl("halcouriershipping/label/printLabel", array('halcourier_label' => $track_number,'track_number' => $track_number));
    }

	public function cleanStr($str)
	{
		$str = trim($str);
		$str = str_replace(
			array('&'),
			array('&amp;'),
		$str);
		return $str;
	}

	public function getHalcourierStatus($track_number, $cron = false)
	{
		$log = Mage::helper('halcouriershipping')->getConfigData('log');
		if(!$cron)
			$ordenId = $this->getOrder()->getIncrementId();
		if(!Mage::helper('halcouriershipping')->loadClass('Soap/nusoap','require') ||
			!Mage::helper('halcouriershipping')->loadClass('Utils/Types') ||
			!Mage::helper('halcouriershipping')->loadClass('Utils'))
		{
			if(!$cron)
				Mage::helper('halcouriershipping')->log('[CONSULTA ESTADOS][ERROR EN LA CARGA DE CLASES] PEDIDO #'.$ordenId,9);
		}
		$login = new LoginTypeIn();
        $login->codUsu = Mage::helper('halcouriershipping')->getConfigData('halcourier_codigo_usuario', $this->getOrder()->getStoreId());
		$login->codAgencia = Mage::helper('halcouriershipping')->getConfigData('halcourier_codigo_agencia', $this->getOrder()->getStoreId());
		$login->codCliente = Mage::helper('halcouriershipping')->getConfigData('halcourier_codigo_cliente', $this->getOrder()->getStoreId());
		$login->password = Mage::helper('core')->decrypt(Mage::helper('halcouriershipping')->getConfigData('halcourier_password_cliente', $this->getOrder()->getStoreId()));
		$login->tipoValidacion = 'Cliente';
		$client = new nusoap_client(Mage::getStoreConfig('carriers/halcourier_avanzadas/halcourier_url_envios', $this->getOrder()->getStoreId()),true);
		$envio = new BusquedaEnviosConEstadoseIncidenciasTypeIn();
		$envio->autenticar = Halcourier_Shipping_Helper_Utils::objectToArray($login);
		$envio->albaran = $track_number;
		$envio->campos = '*';
		$body = Halcourier_Shipping_Helper_Utils::objectsToNusoap($envio->autenticar,$envio,'BusquedaEnviosConEstadoseIncidenciasTypeIn');
		$client->soap_defencoding = 'utf-8';
		$client->useHTTPPersistentConnection();
		$client->setUseCurl(true);
		$response = $client->send($body, 'BusquedaEnviosConEstadoseIncidencias');
		if($log && $cron == false)
		{
			Mage::helper('halcouriershipping')->log('[CONSULTA ESTADOS] '.print_r($envio,true),5);
			Mage::helper('halcouriershipping')->log('[RESPUESTA CONSULTA ESTADOS] '.print_r($response,true),5);
		}
		$errors = false;
		if(isset($response['return']) && is_array($response['return']))
		{
			return $response['return'];
		}
		else
		{
			if(isset($response['returnError']))
				$error_message = iconv("ISO-8859-1","UTF-8", $response['returnError']);
			else
				$error_message = Mage::helper('halcouriershipping')->__('Sin conexión con Halcourier.');
			$errors = true;
		}
		if ($errors == true)
		{
			$error_message = utf8_decode(Mage::helper('halcouriershipping')->__('Albarán no encontrado'));
			if($log && $cron == false)
				Mage::log('Halcourier error: '.$error_message);
			$response['descEstadoActual'] = $error_message;
			return $response;
		}
		return;
	}
}
