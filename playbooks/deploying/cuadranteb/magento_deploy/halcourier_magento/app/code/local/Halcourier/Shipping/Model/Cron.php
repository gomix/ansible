<?php
/**
 * ecommbits.com
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Halcourier
 * @package    Halcourier_Shipping
 * @version    Release: 1.0
 * @author     ecommbits.com (desarrollo@ecommbits.com)
 * @copyright  Copyright (c) 2013 ecommbits.com (http://www.ecommbits.com)
 */

class Halcourier_Shipping_Model_Cron
{	
    public function updateEstados()
    {
        try
        {
        	if(!Mage::helper('halcouriershipping')->getConfigData('halcourier_cron_cerrado'))
				return true;
			$log = Mage::helper('halcouriershipping')->getConfigData('log');
			if($log)
					Mage::helper('halcouriershipping')->log('[INICIO CRON]',5);
			$_orders = $this->_getHalcourierOrders()->getData();
			foreach ($_orders as $_order)
			{
				$order = Mage::getModel('sales/order')->load($_order['entity_id']);
				if (count($order->getShipmentsCollection()->getData()) != 1)
					continue;
				$tracking_number = $this->_getTrackingNumber($order);
				if($tracking_number == false)
					continue;
				$status = Mage::getModel('halcouriershipping/shipment')->getHalcourierStatus($tracking_number, true);
				if(isset($status['estadoActual']) && $status['estadoActual'])
				{
					$estados_finales = explode(',', Mage::getStoreConfig('carriers/halcourier_avanzadas/halcourier_estados_finales'));
					if(in_array($status['estadoActual'], $estados_finales))
					{
						$estadoInicial = $order->getStatusLabel();
						$order->addStatusToHistory(Mage_Sales_Model_Order::STATE_CLOSED, Mage::helper('halcouriershipping')->__('Cambio de estado automático. Pedido entregado por Halcourier.'), false);
						$order->save();
						if($log)
							Mage::helper('halcouriershipping')->log('[CRON] Pedido #'.$order->getIncrementId().' cambiado de estado '.$estadoInicial.' a estado '.$order->getStatusLabel(),5);
					}
				}
			}
			if($log)
					Mage::helper('halcouriershipping')->log('[CRON EJECUTADO CORRECTAMENTE]',5);
        }
        catch(Exception $e)
        {
        	Mage::helper('halcouriershipping')->log('[ERROR CRON]'.' '.$e->getMessage(),5);
        }
    }

    protected function _getHalcourierOrders()
    {
		$_orders = Mage::getModel('sales/order')->getCollection()
					->addAttributeToFilter('status', array('eq' => Mage_Sales_Model_Order::STATE_COMPLETE));
		return $_orders;
    }
	
    protected function _getTrackingNumber($order)
    {
    	$tracknums = array();
		foreach($order->getShipmentsCollection() as $shipment)
		{
			foreach($shipment->getAllTracks() as $track)
				if($track->getCarrierCode() == 'halcourier')
			    	$tracknums[] = $track->getNumber();
			if(count($tracknums) != 1)
				return false;
			else
				return $tracknums[0];
		}
    }
}