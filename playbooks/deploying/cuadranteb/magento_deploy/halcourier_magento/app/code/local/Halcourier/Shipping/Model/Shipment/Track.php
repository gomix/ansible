<?php
/**
 * ecommbits.com
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Halcourier
 * @package    Halcourier_Shipping
 * @version    Release: 1.0
 * @author     ecommbits.com (desarrollo@ecommbits.com)
 * @copyright  Copyright (c) 2013 ecommbits.com (http://www.ecommbits.com)
 */

class Halcourier_Shipping_Model_Shipment_Track extends Mage_Sales_Model_Order_Shipment_Track
{
    protected function _beforeDelete()
    {
		$canDelete = parent::_beforeDelete();
		$log = Mage::helper('halcouriershipping')->getConfigData('log');
        $track = Mage::getModel('sales/order_shipment_track')->load($this->getId());
        if ($track->getId())
        {
			if ($track->getCarrierCode() == 'halcourier')
			{
				$ordenId = $track->getOrderId();
				if(!Mage::helper('halcouriershipping')->loadClass('Soap/nusoap','require') ||
					!Mage::helper('halcouriershipping')->loadClass('Utils/Types') ||
					!Mage::helper('halcouriershipping')->loadClass('Utils'))
				{
					Mage::helper('halcouriershipping')->log('[BORRAR ENVIO][ERROR EN LA CARGA DE CLASES] PEDIDO #'.$ordenId,9);
				}
				$db = Mage::getSingleton('core/resource')->getConnection('core_write');
				$result = $db->query("select track_number from sales_flat_shipment_track where entity_id = '".$track->getId()."' and carrier_code = 'halcourier' limit 1");
				$row = $result->fetch();
				if(count($row) == 0)
					return $canDelete;
				$login = new LoginTypeIn();
                $login->codUsu = Mage::helper('halcouriershipping')->getConfigData('halcourier_codigo_usuario', $this->getStoreId());
				$login->codAgencia = Mage::helper('halcouriershipping')->getConfigData('halcourier_codigo_agencia', $this->getStoreId());
				$login->codCliente = Mage::helper('halcouriershipping')->getConfigData('halcourier_codigo_cliente', $this->getStoreId());
				$login->password = Mage::helper('core')->decrypt(Mage::helper('halcouriershipping')->getConfigData('halcourier_password_cliente', $this->getStoreId()));
				$login->tipoValidacion = 'Cliente';
				$client = new nusoap_client(Mage::getStoreConfig('carriers/halcourier_avanzadas/halcourier_url_envios', $this->getStoreId()),true);
				$envio = new BorrarEnvioTypeIn();
				$envio->autenticar = Halcourier_Shipping_Helper_Utils::objectToArray($login);
				$envio->albaran = $row['track_number'];
				$body = Halcourier_Shipping_Helper_Utils::objectsToNusoap($envio->autenticar,$envio,'BorrarEnvioTypeIn');
				$client->soap_defencoding = 'utf-8';
				$client->useHTTPPersistentConnection();
				$client->setUseCurl(true);
				$response = $client->send($body, 'BorrarEnvio');
				if($log)
				{
					Mage::helper('halcouriershipping')->log('[BORRAR ENVIO] '.print_r($envio,true),5);
					Mage::helper('halcouriershipping')->log('[RESPUESTA BORRAR ENVIO] '.print_r($response,true),5);
				}
				$errors = false;
				if(isset($response['return']) && is_array($response['return']))
				{
					if ($response['return']['codError'] != '0' && $response['return']['codError'] != '2')
					{
						$error_message = '['.$response['return']['codError'].'] '.$response['return']['descripcionError'];
						$errors = true;
					}
					else
					{
						Mage::getSingleton('adminhtml/session')->addSuccess(
							Mage::helper('halcouriershipping')->__('Halcourier: Albarán').' '.$row['track_number'].' '.Mage::helper('halcouriershipping')->__('eliminado correctamente.'));
					}
					if ($response['return']['codError'] == '2')
					{
						Mage::getSingleton('adminhtml/session')->getMessages(true);
						$errors = false;
						Mage::getSingleton('adminhtml/session')->addSuccess(
							Mage::helper('halcouriershipping')->__('Halcourier: El albarán').' '.$row['track_number'].' '.Mage::helper('halcouriershipping')->__('ya estaba eliminado en el sistema.'));
					}
				}
				else
				{
					if(isset($response['faultstring']))
						$error_message = '[99999] '.iconv("ISO-8859-1","UTF-8", $response['faultstring']);
					else
						$error_message = '[99999] '.Mage::helper('halcouriershipping')->__('Error indeterminado. Compruebe la conexión con Halcourier.');
					$errors = true;
				}
				if ($errors == true)
				{
					Mage::log('Halcourier error: '.$error_message);
					Mage::throwException(Mage::helper('halcouriershipping')->__('Halcourier error:').' '.$error_message);					
				}
				else
				{
					$result = $db->query("update sales_flat_shipment set shipping_label='' where entity_id='".$track->getId()."'");
				}
			}
        }
        return $canDelete;
    }
}
