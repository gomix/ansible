#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp*/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
# ENTIPI S.A. 
# Caracas, Venezuela
#
#ddns-update-style interim;
ignore client-updates;
default-lease-time 21600;
max-lease-time 43200;
option time-offset -16200; # VET Standard Time
server-name "dhcp-1.entipi.com";
option domain-name "entipi.com";
option domain-name-servers 192.168.1.20;
option ntp-servers ntp_1.entipi.com, ntp_2.entipi.com;
stash-agent-options true;
authoritative;

#subnet 10.0.1.0 netmask 255.255.255.0 {
#	option ntp-servers ntp_1.entipi.com, ntp_2.entipi.com;         
#	# option netbios-name-servers 192.168.1.100;    # WINS server 
#	# option ipforwarding off;

#log (info, concat( "circuit-id: ", option agent.circuit-id));
#log (debug, concat ("giaddr: ", binary-to-ascii(10,8, ".", packet(24,4)))) ;
#log (info, concat ("giaddr: ", binary-to-ascii(10,8, ".", packet(24,4)))) ;

class "vlan-10.4.10" {
  match if packet(24,4) = 0a:04:0a:01;
}

class "eth0" {
  match if packet(24,4) = 00:00:00:00;
}

shared-network entipi {
  # vlan 10.4.10.0/24
  subnet 10.4.10.0 netmask 255.255.255.0 {
    option subnet-mask 255.255.255.0;
    option broadcast-address 10.4.10.255;
    option domain-name-servers 10.0.1.48;
    option routers 10.4.10.1;      

    pool {
      range 10.4.10.100 10.4.10.254;
      allow members of "vlan-10.4.10";
    }
  }

  # vlan alphanet (10.0.1.0/24)
  subnet 10.0.1.0 netmask 255.255.255.0 {
    option subnet-mask 255.255.255.0;
    option broadcast-address 10.0.1.255;
    option routers 10.0.1.1;      
    pool {
      range 10.0.1.128 10.0.1.254;  
      allow members of "eth0";
    }
  }
}

group 10-4-10-net-hosts {
  host ws_test1 { 
    hardware ethernet c0:3f:d5:aa:39:29; 
    fixed-address 10.4.10.2; 
  }
}

group alphanet-hosts {
        host ds_1 { 
		hardware ethernet 52:54:00:3e:19:82; 
		fixed-address ds_1.entipi.com; 
	}

        host gitlab_1 { 
		hardware ethernet 52:54:00:c6:72:8b; 
		fixed-address 10.0.1.30; 
	}

        host gitlab_devel_1 { 
		hardware ethernet 52:54:00:c6:72:8c; 
		fixed-address 10.0.1.31; 
	}

        host mail_1 { 
		hardware ethernet 52:54:00:c6:72:8d; 
		fixed-address mail_1.entipi.com; 
		option routers 10.0.1.1;      
	}

        host mail-2 { 
		hardware ethernet 52:54:00:f4:a0:a1; 
		fixed-address mail_2.entipi.com; 
	}

        host redmine_devel_1 { 
		hardware ethernet 52:54:00:c6:72:8e; 
		fixed-address 10.0.1.32; 
	}

        host zimbra-1.entipi.com { 
		hardware ethernet 52:54:00:c6:72:8f; 
		fixed-address zimbra-1.entipi.com; 
	}

        host zimbra_2 { 
		hardware ethernet 52:54:00:c6:72:90; 
		fixed-address zimbra_2.entipi.com; 
	}

        # TODO: registrar en DNS
        host proxyr_2 { 
		hardware ethernet 52:54:00:c6:72:91; 
		fixed-address 10.0.1.35; 
	}

        host ds_2 { 
		hardware ethernet 52:54:00:c6:72:92; 
		fixed-address ds_2.entipi.com; 
	}

        host sicor_1 { 
		hardware ethernet 52:54:00:c6:72:93; 
		fixed-address sicor_1.entipi.com; 
	}

        host cadcam_1 { 
		hardware ethernet 52:54:00:c6:72:94; 
		fixed-address 10.0.1.42; 
	}

        host cadcam_2 { 
		hardware ethernet 52:54:00:c6:72:95; 
		fixed-address 10.0.1.43; 
	}

        host generic_1.entipi.com { 
		hardware ethernet 52:54:00:c6:72:96; 
		fixed-address 10.0.1.47; 
	}

        host samba_ad_1.entipi.com { 
		hardware ethernet 52:54:00:c6:72:97; 
		fixed-address 10.0.1.45; 
	}

        host ns-1.entipi.com { 
		hardware ethernet 52:54:00:c6:72:98; 
		fixed-address 10.0.1.48; 
	}

        host builder-1.entipi.com { 
		hardware ethernet 52:54:00:c6:72:99; 
		fixed-address 10.0.1.49; 
	}

        host ipa-1.entipi.com { 
		hardware ethernet 52:54:00:c6:72:9a; 
		fixed-address 10.0.1.50; 
	}

        host samba-sfs-1.entipi.com { 
		hardware ethernet 52:54:00:c6:72:9b; 
		fixed-address samba-sfs-1.entipi.com; 
	}

        host gomix-redmine.entipi.com { 
		hardware ethernet 52:54:00:c6:72:9c; 
		fixed-address 10.0.1.52; 
	}

        host samba-sfs-2.entipi.com {
                hardware ethernet 52:54:00:c6:72:9d;
                fixed-address samba-sfs-2.entipi.com;
	}
        
        host im-1.entipi.com {
                hardware ethernet 52:54:00:c6:72:9e;
                fixed-address 10.0.1.54;
	}

        host gt-1.entipi.com {
                hardware ethernet 52:54:00:c6:72:9f;
                fixed-address 10.0.1.56;
	}

        host cnlt-1.localdomain {
                hardware ethernet 52:54:00:c6:72:a0;
                fixed-address 10.0.1.57;
	}

}
