# Sincroniza nuestra copia local de los repos usando rsync
# 
# Uso del proxy squid
export RSYNC_PROXY=squid_1.entipi.com:3128
#export RSYNC_PROXY=username:password@proxyhost:proxyport

# base no debería tener nuevos paquetes
# base
echo "###############"
echo "[base 7.2] sync"
echo "###############"
rsync -av rsync://mirrors.kernel.org:/centos/7.2.1511/os/x86_64/Packages /var/www/html/yumrepo/centos/7.2.1511/os/x86_64/

# updates 7.2
echo "##################"
echo "[updates 7.2] sync"
echo "##################"
logger -t "sync repos" -i "[updates]"
rsync -av rsync://mirrors.kernel.org:/centos/7.2.1511/updates/x86_64/Packages /var/www/html/yumrepo/centos/7.2.1511/updates/x86_64/

# extras
echo "#################"
echo "[extras 7.2] sync"
echo "#################"
logger -t "sync repos" -i "[extras]"
rsync -av rsync://mirrors.kernel.org:/centos/7.2.1511/extras/x86_64/Packages /var/www/html/yumrepo/centos/7.2.1511/extras/x86_64/

# centosplus
echo "#####################"
echo "[centosplus 7.2] sync"
echo "#####################"
logger -t "sync repos" -i "[centosplus]"
rsync -av rsync://mirrors.kernel.org:/centos/7.2.1511/centosplus/x86_64/Packages /var/www/html/yumrepo/centos/7.2.1511/centosplus/x86_64/

# epel
echo "###########"
echo "[epel] sync"
echo "###########"
logger -t "sync repos" -i "[epel]"
rsync --exclude=repodata --exclude=repoview --exclude=debug -av rsync://mirrors.kernel.org:/fedora-epel/7/x86_64 /var/www/html/yumrepo/fedora-epel/7/

# epel-testing (testing)
echo "#####################"
echo "[epel-testing] sync"
echo "#####################"
logger -t "sync repos" -i "[epel]"
rsync --exclude=repodata --exclude=repoview --exclude=debug -av rsync://mirrors.kernel.org:/fedora-epel/testing/7/x86_64 /var/www/html/yumrepo/fedora-epel/testing/7/

# gitlab_gitlab-ce
echo "##############################"
echo "[gitlab_gitlab-ce_origin] sync"
echo "##############################"
logger -t "sync repos" -i "[gitlab_gitlab-ce_origin]"
reposync -n --norepopath --repoid=gitlab_gitlab-ce_origin -p /var/www/html/yumrepo/gitlab_gitlab-ce/el/7/x86_64/
