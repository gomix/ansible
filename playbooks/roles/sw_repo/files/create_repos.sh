# Crea los repositorios yum

# No actualizo, no deben haber paquetes nuevos en base
# base
#echo "createrepo [base]"
#createrepo /var/www/html/yumrepo/centos/7.1.1503/os/x86_64/Packages/

# updates
echo "#####################"
echo "createrepo [updates]"
echo "#####################"
logger -t "create repos" -i "[updates]"
createrepo /var/www/html/yumrepo/centos/7.2.1511/updates/x86_64/

# extras
echo "#####################"
echo "createrepo [extras]"
echo "#####################"
logger -t "create repos" -i "[extras]"
createrepo /var/www/html/yumrepo/centos/7.2.1511/extras/x86_64/

# centosplus
echo "#####################"
echo "createrepo [centosplus]"
echo "#####################"
logger -t "create repos" -i "[centosplus]"
createrepo /var/www/html/yumrepo/centos/7.2.1511/centosplus/x86_64/

# epel
echo "#####################"
echo "createrepo [epel]"
echo "#####################"
logger -t "create repos" -i "[epel]"
createrepo --update /var/www/html/yumrepo/fedora-epel/7/x86_64/

# epel-testing
echo "#####################"
echo "createrepo [epel-testing]"
echo "#####################"
logger -t "create repos" -i "[epel-testing]"
createrepo --update /var/www/html/yumrepo/fedora-epel/testing/7/x86_64/

# gitlab-ce
echo "#####################"
echo "createrepo [gitlab-ce]"
echo "#####################"
logger -t "create repos" -i "[gitlab-ce]"
createrepo /var/www/html/yumrepo/gitlab_gitlab-ce/el/7/x86_64/
