Role Name
=========

certbot role: Automatically enable HTTPS on your website with EFF's Certbot, deploying Let's Encrypt certificates.

Scope: Centos 7 but looking forward to make it available for other Linux variants.

Requirements
------------

None that im aware of, yet.

Role Variables
--------------

Developing, have some patience.

* defaults/main.yml
* vars/main.yml, 
* and any variables that can/should be set via parameters to the role. 
* any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

Dependencies
------------

None that im aware of, yet.

Example Playbook
----------------

Developing, have some patience.

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

MIT

Author Information
------------------

Guillermo Gómez Savino (guillermo.gomez@gmail.com)

