# Crea los repositorios yum

# centos 7.2 base
echo "############################"
echo "createrepo [centos 7.2 base]"
echo "############################"
logger -t "create repos" -i "[centos 7.2 base]"
createrepo /var/www/html/yumrepo/centos/7.2.1511/os/x86_64/Packages/

# centos 7.2 updates
echo "###############################"
echo "createrepo [centos 7.2 updates]"
echo "###############################"
logger -t "create repos" -i "[centos 7.2 updates]"
createrepo /var/www/html/yumrepo/centos/7.2.1511/updates/x86_64/

# centos 7.2 extras
echo "##############################"
echo "createrepo [centos 7.2 extras]"
echo "##############################"
logger -t "create repos" -i "[centos 7.2 extras]"
createrepo /var/www/html/yumrepo/centos/7.2.1511/extras/x86_64/

# centos 7.2 centosplus
echo "##################################"
echo "createrepo [centos 7.2 centosplus]"
echo "##################################"
logger -t "create repos" -i "[centos 7.2 centosplus]"
createrepo /var/www/html/yumrepo/centos/7.2.1511/centosplus/x86_64/

# epel 7
echo "###################"
echo "createrepo [epel 7]"
echo "###################"
logger -t "create repos" -i "[epel 7]"
createrepo --update /var/www/html/yumrepo/fedora-epel/7/x86_64/

# epel-testing 7
echo "###########################"
echo "createrepo [epel-testing 7]"
echo "###########################"
logger -t "create repos" -i "[epel-testing 7]"
createrepo --update /var/www/html/yumrepo/fedora-epel/testing/7/x86_64/

# fedora 24 
echo "###########################"
echo "createrepo [fedora 24]"
echo "###########################"
logger -t "create repos" -i "[fedora 24]"
createrepo --update /var/www/html/yumrepo/fedora/releases/24/Everything/x86_64/os/Packages

# fedora 24 updates
echo "##############################"
echo "createrepo [fedora 24 updates]"
echo "##############################"
logger -t "create repos" -i "[fedora 24 updates]"
createrepo --update /var/www/html/yumrepo/fedora/updates/24/x86_64
