#!/usr/bin/bash
# Actualización de los repos
/usr/local/bin/rsync_repos.sh
# Creación de metadata de los repos
/usr/local/bin/create_repos.sh
# Reajuste selinux de los archivos del repo
/usr/local/bin/selinux_repofiles.sh

#### Mejoras posibles
# 1. Actualización de repos por tema de seguridad
#
#  Al aparecer un parche de seguridad para algún problema (CVE)
#  Los repos deberían actualizarse completamente
#  Entonces lo que se necesita es vigilar la aparición de nuevos
#  CVEs o ser notificado de alguna manera para forzar la actualización
#  de los repositorios, de resto, solo nos enteraremos "al día siguiente"
#  ya que la actualización via cron ocurre a media noche
