# Sincroniza nuestra copia local de los repos usando rsync
# 
# Uso del proxy squid
#export RSYNC_PROXY=username:password@proxyhost:proxyport

# base no debería tener nuevos paquetes
# TODO: run external script
# for data in /var/www/html/yumrepos/repos |repo|
#   echo "mensajes"
#   logger "mensajes" 
#   action rsync options orig destino
# end

for i in $( ls /var/www/yumrepo/repos ); do
  echo item: $i
done

# updates 7.2
#echo "#########################"
#echo "[centos 7.2 updates] sync"
#echo "#########################"
#logger -t "sync repos" -i "[centos 7.2 updates]"
#rsync -av rsync://mirrors.kernel.org:/centos/7.2.1511/updates/x86_64/Packages /var/www/html/yumrepo/centos/7.2.1511/updates/x86_64/

# extras
#echo "########################"
#echo "[centos 7.2 extras] sync"
#echo "########################"
#logger -t "sync repos" -i "[centos 7.2 extras]"
#rsync -av rsync://mirrors.kernel.org:/centos/7.2.1511/extras/x86_64/Packages /var/www/html/yumrepo/centos/7.2.1511/extras/x86_64/

# centosplus
#echo "############################"
#echo "[centos 7.2 centosplus] sync"
#echo "############################"
#logger -t "sync repos" -i "[centos 7.2 centosplus]"
#rsync -av rsync://mirrors.kernel.org:/centos/7.2.1511/centosplus/x86_64/Packages /var/www/html/yumrepo/centos/7.2.1511/centosplus/x86_64/

# epel
#echo "###########"
#echo "[epel] sync"
#echo "###########"
#logger -t "sync repos" -i "[epel]"
#rsync --exclude=repodata --exclude=repoview --exclude=debug -av rsync://mirrors.kernel.org:/fedora-epel/7/x86_64 /var/www/html/yumrepo/fedora-epel/7/

# epel-testing (testing)
#echo "#####################"
#echo "[epel-testing] sync"
#echo "#####################"
#logger -t "sync repos" -i "[epel]"
#rsync --exclude=repodata --exclude=repoview --exclude=debug -av rsync://mirrors.kernel.org:/fedora-epel/testing/7/x86_64 /var/www/html/yumrepo/fedora-epel/testing/7/

# fedora 24 everything x86_64
#echo "##################################"
#echo "[fedora 24 everything x86_64] sync"
#echo "##################################"
#logger -t "sync repos" -i "[fedora 24 everything x86_64]"
#rsync --exclude=repodata --exclude=repoview --exclude=debug -av rsync://mirrors.kernel.org:/fedora/releases/24/Everything/x86_64/os/Packages /var/www/html/yumrepo/fedora/releases/24/Everything/x86_64/os/

# fedora 24 updates x86_64
#echo "###############################"
#echo "[fedora 24 updates x86_64] sync"
#echo "###############################"
#logger -t "sync repos" -i "[fedora 24 updates x86_64]"
#rsync --exclude=repodata --exclude=repoview --exclude=debug -av rsync://mirrors.kernel.org:/fedora/updates/24/x86_64 /var/www/html/yumrepo/fedora/updates/24/

